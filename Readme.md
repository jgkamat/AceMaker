#AceMaker, by Andrew and Jay

[![Build Status](https://circleci.com/gh/jgkamat/AceMaker.svg?&style=svg)](https://circleci.com/gh/jgkamat/AceMaker)

AceMaker is a multiplayer/singleplayer 2D flight simulator, based off of semi-realistic lift mechanics, as  well as Newton's 2nd law. It's a side scrolling game we hope you'll enjoy. Future updates may include more weapons, enemies, menus, powerups, tutorials, and more. Anyone would love to use our program! You will be able to [take off, land], crash, blow up, and stall.
(Subject to change without notice. Authors withold the right to silently curse you if you don't read this. This is by no means gurentees any updates.)

AS OF PRE-RELEASE 0.01
 Use a commandArgument -server to start the server.

### Responsibilities:
Jay-Backend (deciding where things need to go, and how)
Andrew-Frontend, Networking (making a way to see where the things are going. Communications between computers so people can play the game)

### Library Requirements:
Swing, JSoundPlayer

Main -where the program starts
BulletEntity represents and emulates a bullet
Entity- a nonmoving object
EntityManager- manages and distributes entities
FlyingEntity- Represents planes
HomingEntity- missles
MovingEntity- a normal entity that can move
TurretEntity- a stationary turret
AceMaker- Represents client gui
CoolButton- sure is a cool butto
GameGui- Represents and run's the client's GUI
Title- titlescreen
TitleImage- The changing title images
Client- The network side of client
Command- The most basic type of network communication
CommandDisconnect-dur
commandGiveEntity-lets the client give its entity over to the server
CommandHandshake-server gives client the world
CommandMove- client gives server where it's plane has moved
CommandUpdate-server gives client what has changed around them
NetworkServer- Tools to help with the sending + reciving on the network
ServerToClientConnection-represents a server to client connection, allows transfer of stuff
UpdateThread-The server's infinite loop+bunch of utilities
Vector2D-provides everything you could want with vectors, from distance to angles, to addition
F22- an advanced high performance jet.
JSoundPlayer- Plays+manages sounds
Util-utilites and settings for the game

All of the classes are better covered in the UML and JavaDoc

### Instructions:
Press A to twist left, and D to twist right.
Press W to thrust forward.
Press esc to exit.
Press J to fire bullets
Press k to launch missiles

N/A
~Press l to bomb

#### Must have:
1. Network		(COMPLETE) --With minor bugs
2. Fly		(COMPLETE) --With Bugs
3. scroll		(COMPLETE) --with minor bugs
4. Crash		(IN PROGRESS)
5. shooting		(COMPLETE)
6. sound		(NEARLY COMPLETE) --sound effects

#### Want to have:
1. AI for computer	(PLANNING)
2. health		(PLANNING)
3. bombs		(PLANNING)
4. powerups		(IDEA DISCARDED)
5. Lots of weapons	(IN PROGRESS)

#### Stretch Features:
1. Super hard AI		(IDEA DISCARDED)
2. Bug free 3D		(IDEA DISCARDED -for now)

### Questions/concerns/criticisms

1. No class descriptions.
	(RESOLVED IN JAVADOCS)

2. UML is too tightly packed together/difficult to comprehend.
	(ISSUE LABELED TOO DIFFICULT TO RESOLVE)

3. Main class is not attached to any other classes.
	(ISSUE RESOLVED)

4. Command and COmmandParser classes are floating.
	(ISSUE RESOLVED)

5. I assume that this is some kind of side scroller, but you could be a little more clear about how the game will look.
	(THIS ISSUE IS BEING ADDRESSED)

## Compiling

1. `gradle run` to run the normal game
2. `gradle fatjar` to build the jar file used for server/client operation.


### Questions? Comments? Concerns?

Contact one of us at berg_tung.a@gmail.com or github@jgkamat.33mail.com, and we'll pretend to be happy to work with you on the issue.
