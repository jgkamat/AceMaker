package org.log;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.logging.StreamHandler;

/**
 * 
 * https://blogs.oracle.com/nickstephen/entry/java_redirecting_system_out_and
 * @author nstephen
 */
public class LoggingUtil {

	public static void setup() throws SecurityException, IOException {
		// preserve old stdout/stderr streams in case they might be useful
		PrintStream stdout = System.out;
		PrintStream stderr = System.err;

		// initialize logging to go to rolling log file
		LogManager logManager = LogManager.getLogManager();
		logManager.reset();

		// log file max size 10K, 3 rolling files, append-on-open
		Handler fileHandler = new FileHandler("log", 10000, 3, true);
		fileHandler.setFormatter(new SimpleFormatter());
		Logger.getLogger("").addHandler(fileHandler);
		Logger.getLogger(Logger.GLOBAL_LOGGER_NAME).addHandler(
				new SystemStreamHandler(stderr, new SimpleFormatter()));

		// now rebind stdout/stderr to logger
		Logger logger;
		LoggingOutputStream los;

		logger = Logger.getLogger("stdout");
		logger.addHandler(new SystemStreamHandler(stdout, new SimpleFormatter()));
		los = new LoggingOutputStream(logger, StdOutErrLevel.STDOUT);
		System.setOut(new PrintStream(los, true));

		logger = Logger.getLogger("stderr");
		logger.addHandler(new SystemStreamHandler(stderr, new SimpleFormatter()));
		los = new LoggingOutputStream(logger, StdOutErrLevel.STDERR);
		System.setErr(new PrintStream(los, true));

	}

	private static class SystemStreamHandler extends StreamHandler {
		public SystemStreamHandler(OutputStream out, Formatter f) {
			super(out, f);
		}

		@Override
		public void publish(LogRecord record) {
			super.publish(record);
			flush();
		}

		@Override
		public void close() {
			flush();
		}
	}

}
