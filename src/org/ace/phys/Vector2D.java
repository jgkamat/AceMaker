package org.ace.phys;

import java.io.Serializable;



/**
 * 
 * @author Jay Kamat
 * @version 2013.5.19
 * A class for representing vectors in a 2D space. Also has a bunch of helpful parts in it to help
 * in physics calculations, such as finding the resultant of two vectors.
 * 
 * This was stolen and upgraded from the JPlanetSimulator project. 
 * 
 */
public class Vector2D implements Serializable{
	//this is the CORE of the program
    //the basis for ALL calculations
    //lets just say removing this would be hard
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -8036943827760380124L;
	private double x1,y1,x2,y2;
    private boolean point; //states if the vector is just a point, or a vector with no length
    
    /**
     * Makes a vector. This constructor is the most accurate and the quickest one, so use it as much as possible
     * @param x1s The starting x coordinate
     * @param y1s The starting y coordinate
     * @param x2s The ending x coordinate
     * @param y2s The ending y coordinate
     */
    public Vector2D(double x1s,double y1s,double x2s,double y2s){ //starting coords is x1,y1 end coords is x2,y2
        x1=x1s;
        x2=x2s;
        y1=y1s;
        y2=y2s;
        if(x1==x2&&y1==y2){
            point=true;
        }
        else point=false;
    }
    
    public Vector2D(){
    	point=true;
    	x1=0;
        x2=0;
        y1=0;
        y2=0;
    }
    
    /**
     * A constructor that uses the start and end points for reference, and finds the true
     * ending points by using the distance
     * @param x1s The starting x coordinate
     * @param y1s The starting y coordinate
     * @param x2s The ending x coordinate (for direction)
     * @param y2s The ending y coordinate (for direction)
     * @param distance The length of the vector
     */
    public Vector2D(double x1s,double y1s,double x2s,double y2s,double distance){ //uses points as direction, and distance...
        double xn;
        double yn;
        double angle;//below creates an angle based on polar theta
        angle=Math.atan2((y2s-y1s),(x2s-x1s)); 
        xn=distance*Math.cos(angle);
        yn=distance*Math.sin(angle); //before:yn=-distance*Math.sin(angle);?
        x1=x1s;
        y1=y1s;
        
        x2=x1+xn;
        y2=y1+yn;
        
        if(x1==x2&&y1==y2){
            point=true;
        }
        else point=false;
    }
    
    /**
     * Constructor that uses points to start with, but uses an angle and length to determine the ending distance.
     * THIS ANGLE IS IN RADIANS. DO NOT PUT DEGREES IN HERE
     * try not to use this constructor. IT MAY BE BUGGED
     * @param x1s The starting x coordinate
     * @param y1s The starting y coordinate
     * @param angle The angle on which to go, in standard angle form.
     * @param distance The length of the vector
     * @param justToMakeConstructorsDifferent Put whatever here. It dosent matter.
     */
    public Vector2D(double x1s,double y1s, double angle, double distance, boolean justToMakeConstructorsDifferent){
    	
        double xn;
        double yn;
        
        x1=x1s;
        y1=y1s;
        
        xn=distance*Math.cos(angle);
        yn=distance*Math.sin(angle);
        
        x2=x1+xn;
        y2=y1+yn;
        
        if(x1==x2&&y1==y2){
            point=true;
        }
        else point=false;
    }
    
    /**
     * Finds the resultant of two vectors and dumps it in the vector you are calling this method on.
     * THESE VECTORS MUST START FROM THE SAME POINT
     * @param other The vector to add with
     */
    public void resultant(Vector2D other){ 
        //they must start from the same point
        double xSet;
        double ySet;
        xSet=this.getX()+other.getX();
        ySet=this.getY()+other.getY();
        x2=x1+xSet;
        y2=y1+ySet;
    }
    
    /**
     * Is this a point?
     * @return A unknown value
     */
    public boolean isPoint(){
        return point;
    }
    
    /**
     * A modified version of resultant to help with arrays of vectors. It puts the resultant in the 
     * vector you are calling it on.
     * @param other A bunch of vectors to add with
     */
    public void allResultant(Vector2D[] other){ //take an array of vectors and set result to first
        for(int counter=0;counter<other.length;counter++){
            if(other[counter]!=null&&this!=null)
                this.resultant(other[counter]);
        }
    }
    
    /**
     * Gets the length
     * @return the length
     */
    public double getLength(){
        if(point==true){
            return 0;
        }
        return (Math.hypot(getX(),getY()));
    }
    
    /**
     * Gets the X COMPONENT. NOT THE COORDINATE
     * @return the x COMPONENT. NOT THE COORDINATE
     */
    public double getX(){ 
        return x2-x1;
    }
    
    /**
     * Gets the Y COMPONENT. NOT THE COORDINATE
     * @return The Y COMPONENT. NOT THE COORDINATE
     */
    public double getY(){
        return y2-y1;
    }
    
    /**
     * Gets the starting x coordinate
     * @return the starting x coordinate
     */
    public double getX1(){
        return x1;
    }
    
    /**
     * Gets the starting y coordinate
     * @return the starting y coordinate
     */
    public double getY1(){
        return y1;
    }
    
    /**
     * Gets the ending y coordinate
     * @return the ending y coordinate
     */
    public double getY2(){
        return y2;
    }
    
    /**
     * Gets the ending x coordinate
     * @return the ending x coordinate
     */
    public double getX2(){
        return x2;
    }
    
    /**
     * Finds the direction of the vector, in radians.
     * @return the direction of the vector
     */
    public double getDirection(){ 
        double tmp= ((Math.atan2(getY(),getX()))); 
        if(tmp<0)
        	tmp+=2*Math.PI;
        return tmp;
    }
    
    /**
     * Sets the vector to whatever you want
     * @param x1s The starting x coordinate
     * @param y1s The starting y coordinate
     * @param x2s The ending x coordinate
     * @param y2s The ending y coordinate
     */
    public void set(double x1s,double y1s,double x2s,double y2s){
        x1=x1s;
        x2=x2s;
        y1=y1s;
        y2=y2s;
        if(x1==x2&&y1==y2){
            point=true;
        }
        else point=false;
    }
    
    /**
     * Keeps direction, sets distance.
     * Returns a new vector. Does not change the original 
     * @param distance The distance of the new one
     * @return The new vector
     */
    public Vector2D setDistance(double distance){
        return new Vector2D(x1,y1,x2,y2,distance);
    }
    
    
    /**
     * 
     * Gets the distance without making a vector. Neat!
     * @param x1s The starting x coordinate
     * @param y1s The starting y coordinate
     * @param x2s The ending x coordinate
     * @param y2s The ending y coordinate
     * @return the distance
     */
    public static double getDistance(double x1s, double y1s, double x2s, double y2s){
        //gets traveling distance for any coordanates
        return (Math.hypot((x2s-x1s),(y2s-y1s)));
    }
    
    public static double getDirection(double x1s, double y1s, double x2s, double y2s){
    	double temp= Math.atan2((y2s-y1s),(x2s-x1s));
    	if(temp<0)
    		temp+=Math.PI*2;
    	return temp;
    }
    
    /**
     * If you want to change direction of a vector without altering its length.
     * Nice for things like SWING UPSIDE DOWN COORDINATES
     * @param direction
     */
    public void setDirection(double direction){
    	double xn;
        double yn;
        
        xn=this.getLength()*Math.cos(direction);
        yn=this.getLength()*Math.sin(direction);
        
        x2=x1+xn;
        y2=y1+yn;
        
        if(x1==x2&&y1==y2){
            point=true;
        }
        else point=false;
        
    }
    
    /**
     * Some technical nonsense
     */
    public String toString(){
        return "Vector2D of ("+x1+","+y1+") ("+x2+","+y2+")";
    }

}
