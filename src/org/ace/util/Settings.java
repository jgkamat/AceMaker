package org.ace.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;

/**
 * A bunch of server specific settings. A bunch of getters and setters. 
 * Whatever you want to call it, it dosent need to be javadoc'd.
 * @author jay
 *
 */
public class Settings implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private static double g=0.4; 
	private static double airResistanc=0; 
	private static double airDensit=1;
	public static final double bulletMass=1,bulletSpeed=5;
	private static String texturePack;
	public static int width, height;
	
	public static double getG() {
		return g;
	}
	
	
	
	public static double getAirResistance() {
		return airResistanc;
	}
	public static void setAirResistance(double airResistance) {
		airResistanc = airResistance;
	}

	public static double getAirDensity() {
		return airDensit;
	}

	public static void setAirDensity(double airDensity) {
		airDensit = airDensity;
	}
	/**
	 * Gets the default port.
	 * @return 4444
	 */
	public static int getDefaultPort() {
		return 4444;
	}

	/**
	 * Well... NOT IMPLEMENETED YET
	 * 
	 * @return Always returns null.
	 */
	public static String getSaveLocation() {
		return null;
	}

	/**
	 * Max people. In the future, we will set it to:
	 * Shelby.getRoom().getCurrentOccupancy()-1;
	 * 
	 * @return the max people.
	 */
	public static int getMaxPeople() {
		return 40;
	}
	
	public void setTP(String s) throws FileNotFoundException{
		File f = new File(s);
		if(f.exists() && f.isDirectory())
			texturePack = s;
		else
			throw new FileNotFoundException(s);
	}
	
	public static String getTP(){
		return texturePack;
	}



	public static int getWidth() {
		return width;
	}



	public static void setWidth(int width) {
		Settings.width = width;
	}



	public static int getHeight() {
		return height;
	}



	public static void setHeight(int height) {
		Settings.height = height;
	}
}
