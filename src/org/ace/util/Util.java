package org.ace.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Transparency;

import java.awt.event.MouseMotionListener;

import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.PixelGrabber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;

import java.security.CodeSource;

import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import java.util.zip.ZipInputStream;
import java.util.zip.ZipEntry;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.imageio.ImageIO;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.UIManager;

import javax.swing.plaf.basic.BasicButtonListener;


/**
 * All the defaults and various utilities are here.
 * 
 * @author Andrew
 * 
 */
public class Util {
	public static final Random random = new Random();

	private static double FACTOR = .80;

	public static Color darker(Color c) {
		return darker(c, FACTOR);
	}

	public static Color darker(Color c, double factor) {
		return new Color(Math.max((int) (c.getRed() * factor), 0), Math.max(
				(int) (c.getGreen() * factor), 0), Math.max(
				(int) (c.getBlue() * factor), 0), c.getAlpha());
	}

	public static Color brighter(Color c) {
		return brighter(c, FACTOR);
	}

	public static Color brighter(Color c, double factor) {
		int r = c.getRed();
		int g = c.getGreen();
		int b = c.getBlue();
		int alpha = c.getAlpha();

		/*
		 * From 2D group: 1. black.brighter() should return grey 2. applying
		 * brighter to blue will always return blue, brighter 3. non pure color
		 * (non zero rgb) will eventually return white
		 */
		int i = (int) (1.0 / (1.0 - factor));
		if (r == 0 && g == 0 && b == 0) {
			return new Color(i, i, i, alpha);
		}
		if (r > 0 && r < i)
			r = i;
		if (g > 0 && g < i)
			g = i;
		if (b > 0 && b < i)
			b = i;

		return new Color(Math.min((int) (r / factor), 255), Math.min(
				(int) (g / factor), 255), Math.min((int) (b / factor), 255),
				alpha);
	}

	/**
	 * Listens for button clicks
	 * @param b
	 * @return
	 */
	public static BasicButtonListener getButtonListener(AbstractButton b) {
		MouseMotionListener[] listeners = b.getMouseMotionListeners();

		if (listeners != null) {
			for (MouseMotionListener listener : listeners) {
				if (listener instanceof BasicButtonListener) {
					return (BasicButtonListener) listener;
				}
			}
		}
		return null;
	}

	/**
	 * Remember that fancy readme-veiewer in the program?
	 * @return
	 */
	public static String getReadme() {
		File f = new File("").getAbsoluteFile();
		File[] farr = f.listFiles();
		for (File t : farr) {
			if (!t.isDirectory()
					&& t.getName().toLowerCase().startsWith("readme")) {
				f = t;
				break;
			}
		}
		if (f.isDirectory())
			return "Readme could not be found";
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(f);
			//byte[] buffer = new byte[1000];
			int i;
			StringBuilder sb = new StringBuilder();
			while ((i = fis.read()) != -1) {
				sb.append((char)i);
			}
			return sb.toString();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "Readme could not be found";
		} catch (IOException e) {
			e.printStackTrace();
			return "Error reading " + f.getName() + "We're sorry";
		} finally {
			if (fis != null)
				try {
					fis.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
		}
	}

	public static final Logger global = Logger
			.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void logW(String s, Throwable e) {
		Throwable t = new Throwable();
		StackTraceElement ste = t.getStackTrace()[1];
		
		LogRecord lr = new LogRecord(Level.WARNING, s);
		lr.setThrown(e);
		lr.setSourceClassName(ste.toString());
		//lr.setSourceClassName(ste.getClassName());
		//lr.setSourceMethodName(ste.getMethodName());
		
		global.log(lr);
		//global.log(Level.WARNING, s, e);
	}

	public static void logW(String s) {
		Throwable t = new Throwable();
		StackTraceElement ste = t.getStackTrace()[1];
		
		LogRecord lr = new LogRecord(Level.WARNING, s);
		lr.setSourceClassName(ste.toString());
		
		global.log(lr);
	}

	public static void logI(String s) {
		Throwable t = new Throwable();
		StackTraceElement ste = t.getStackTrace()[1];
		
		LogRecord lr = new LogRecord(Level.INFO, s);
		lr.setSourceClassName(ste.toString());
		//lr.setSourceClassName(ste.getClassName());
		//lr.setSourceMethodName(ste.getMethodName());
		
		global.log(lr);
	}

	//private static final File location = new File(
	//		new File("").getAbsoluteFile(), "res");

	/**
	 * Image--> BufferedImage
	 * 
	 * @param image
	 * @return a bufferedImage version of the passed in image.
	 */
	public static BufferedImage toBufferedImage(Image image) {
		if (image instanceof BufferedImage)
			return (BufferedImage) image;
		// Determine if the image has transparent pixels; for this method's
		// implementation, see Determining If an Image Has Transparent Pixels
		boolean hasAlpha = hasAlpha(image);

		// Create a buffered image with a format that's compatible with the
		// screen
		BufferedImage bimage = null;
		GraphicsEnvironment ge = GraphicsEnvironment
				.getLocalGraphicsEnvironment();
		try {
			// Determine the type of transparency of the new buffered image
			int transparency = Transparency.OPAQUE;
			if (hasAlpha) {
				transparency = Transparency.BITMASK;
			}

			// Create the buffered image
			GraphicsDevice gs = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gs.getDefaultConfiguration();
			bimage = gc.createCompatibleImage(image.getWidth(null),
					image.getHeight(null), transparency);
		} catch (HeadlessException e) {
			// The system does not have a screen
		}

		if (bimage == null) {
			// Create a buffered image using the default color model
			int type = BufferedImage.TYPE_INT_RGB;
			if (hasAlpha) {
				type = BufferedImage.TYPE_INT_ARGB;
			}
			bimage = new BufferedImage(image.getWidth(null),
					image.getHeight(null), type);
		}

		// Copy image to buffered image
		Graphics g = bimage.createGraphics();

		// Paint the image onto the buffered image
		g.drawImage(image, 0, 0, null);
		g.dispose();

		return bimage;
	}

	/**
	 * Got alpha?
	 * 
	 * @param image
	 * @return true if the image most likely has alpha. This is always reliable
	 *         for BufferedImages, and usually reliable for other types.
	 */
	public static boolean hasAlpha(Image image) {
	// If buffered image, the color model is readily available
		if (image instanceof BufferedImage) {
			BufferedImage bimage = (BufferedImage) image;
			return bimage.getColorModel().hasAlpha();
		}

		// Use a pixel grabber to retrieve the image's color model;
		// grabbing a single pixel is usually sufficient
		PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
		}

		// Get the image's color model
		ColorModel cm = pg.getColorModel();
		return cm.hasAlpha();
	}

	// http://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
	/**
	 * Gets a scaled version of an image
	 * HAS BUGS.
	 * 
	 * @param img
	 * @param targetWidth
	 * @param targetHeight
	 * @param hint
	 * @param higherQuality
	 *            true if shrinking
	 * @return A scaled version of the BufferedImage
	 */
	@Deprecated
	public static BufferedImage getScaledInstance(BufferedImage img,
			int targetWidth, int targetHeight, Object hint,
			boolean higherQuality) {
		int type = (img.getTransparency() == Transparency.OPAQUE) ? BufferedImage.TYPE_INT_RGB
				: BufferedImage.TYPE_INT_ARGB;
		BufferedImage ret = (BufferedImage) img;
		int w, h;
		if (higherQuality) {
			// Use multi-step technique: start with original size, then
			// scale down in multiple passes with drawImage()
			// until the target size is reached
			w = img.getWidth();
			h = img.getHeight();
		} else {
			// Use one-step technique: scale directly from original
			// size to target size with a single drawImage() call
			w = targetWidth;
			h = targetHeight;
		}

		do {
			if (higherQuality && w > targetWidth) {
				w /= 2;
				if (w < targetWidth) {
					w = targetWidth;
				}
			}

			if (higherQuality && h > targetHeight) {
				h /= 2;
				if (h < targetHeight) {
					h = targetHeight;
				}
			}

			BufferedImage tmp = new BufferedImage(w, h, type);
			Graphics2D g2 = tmp.createGraphics();
			g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
			g2.drawImage(ret, 0, 0, w, h, null);
			g2.dispose();

			ret = tmp;
		} while (w != targetWidth || h != targetHeight);

		return ret;
	}

	/**
	 * Useful tool to make ImageIcons. Prints to the system error log if there
	 * is a problem.
	 * 
	 * @param path
	 * @param description
	 * @return An ImageIcon for the given path and description. null if a
	 *         problem otherwise.
	 */
	public static ImageIcon createImageIcon(String path, String description) {
		try {
			File f = new File("res/" + path);
			URL imgURL = f.toURI().toURL();
			if (imgURL != null) {
				return new ImageIcon(imgURL, description);
			} else {
				System.err
						.println("Couldn't find file: " + f.getAbsolutePath());
				return null;
			}
		} catch (MalformedURLException e) {
			System.out.println("Unknown error occured.");
			return null;
		}
	}

	/**
	 * Gets a bufferedImage from a file
	 * 
	 * @param name
	 * @return A BufferedImage loaded in from res/ + name
	 */
	public static BufferedImage getImage(String name) {
		try {
			File d = new File("res/" + name);
			URL imgURL = d.toURI().toURL();
			BufferedImage neg = null;
			if (imgURL != null) {
				try {
					neg = ImageIO.read(imgURL);
				} catch (IOException e) {
					try{
						BufferedImage i = doJarGet(name);
					if(i != null)
						return i;
					}catch(IOException ioe){
						ioe.printStackTrace();
					}
					System.err.println("Couldn't find file: "
							+ d.getAbsolutePath());
				}
			}
			return neg;
		} catch (java.net.MalformedURLException e) {
			System.out.println("Unknown error occured.");
			return null;
		}
	}
	private static BufferedImage doJarGet(String name) throws IOException{
		name = "res/" + name;
		CodeSource src = Util.class.getProtectionDomain().getCodeSource();
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<BufferedImage> list1 = new ArrayList<BufferedImage>();
		if( src != null ) {
			ZipInputStream zip = null;
			try{
			    URL jar = src.getLocation();
			    zip = new ZipInputStream( jar.openStream());
			    ZipEntry ze = null;
	
			    while( ( ze = zip.getNextEntry() ) != null ) {
			    	if(ze.getName().equals(name))
			    		return ImageIO.read(zip);
			    }
			} finally {
				if(zip != null)
					zip.close();
			}
	    }
		return null;
	}
	
	/**
	 * Gets a BufferedImage of a random color.
	 * 
	 * @param x
	 *            The width of the image.
	 * @param y
	 *            The Height of the image
	 * @return A BufferedImage filled with a random color.
	 */
	public static BufferedImage getRandomSCImage(int x, int y){
		BufferedImage bi = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.createGraphics();
		g.setColor(new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255)));
		g.fillRect(0, 0, x, y);
		g.dispose();
		return bi;
	}

	/**
	 * Gets a BufferedImage where each pixel is a random color.
	 * 
	 * @param x
	 *            The width of the image.
	 * @param y
	 *            The height of the image.
	 * @return A BufferedImage filled with random colors.
	 */
	public static BufferedImage getRandomMCImage(int x, int y){
		BufferedImage bi = new BufferedImage(x, y, BufferedImage.TYPE_INT_RGB);
		Graphics g = bi.createGraphics();
		for(int yl = 0; yl < y; yl++)
			for(int xl = 0; xl <= x; xl++){
				g.drawLine(xl, yl, xl+1, yl+1);
				g.setColor(new Color(random.nextInt(255),random.nextInt(255),random.nextInt(255)));
			}
		g.dispose();
		return bi;
	}
	
	
	private static final FilenameFilter imgs = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			return name.endsWith(".PNG") || name.endsWith(".png")
					|| name.endsWith(".JPG") || name.endsWith(".jpg")
					|| name.endsWith(".JPEG") || name.endsWith(".jpeg");
		}
	};
	/**
	 * Gets ALL buffedImages in a directory with fileExtention .png .jpg or .jpeg
	 * 
	 * @param location
	 * @param require
	 * @return All the images within a directory
	 */
	public static BufferedImage[] getAllImages(String location,
			Component require) {
		File f = new File(location);
		BufferedImage[] result = null;
		boolean doThing = true;
		if (!f.exists() || !f.isDirectory()){
			try{
				result = doJarIMG(location, imgs);
				doThing = false;
			}catch(IOException ioe){
				throw new RuntimeException(ioe);
			}
			if(result == null)
				throw new IllegalArgumentException();
		}
		MediaTracker t;
		if(doThing){
			File[] arr = f.listFiles(imgs);
			t = new MediaTracker(require);
			result = new BufferedImage[arr.length];
			for (int i = 0; i < arr.length; i++) {
				try {
					result[i] = ImageIO.read(arr[i]);
				} catch (IOException e) {
					throw new RuntimeException(e);
				}
				t.addImage(result[i], i);
			}
		}else{
			t = new MediaTracker(require);
			for (int i = 0; i < result.length; i++) {
				t.addImage(result[i], i);
			}
		}
		boolean fail = false;
		try {
			t.waitForAll();
		} catch (InterruptedException e) {
			fail = true;
		}
		if (t.isErrorAny() || fail)
			throw new RuntimeException();
		return result;
	}

	
	private static final FilenameFilter jpg = new FilenameFilter() {
		public boolean accept(File dir, String name) {
			return name.endsWith(".JPG") || name.endsWith(".jpg")
					|| name.endsWith(".JPEG") || name.endsWith(".jpeg");
		}
	};
	/**
	 * Gets all the JPG's in the given directory. Throws exception rather then
	 * returning null for any errors.
	 * 
	 * @param location
	 *            the directory
	 * @param require
	 *            The component these images should be loaded for.
	 * @return An array of BufferedImages within the directory.
	 */
	public static BufferedImage[] getJPGImages(String location,
			Component require) {
		File f = new File(location);
		// System.out.println(f.getAbsolutePath());
		if (!f.exists() || !f.isDirectory()){
			try {
				return doJarIMG(location, jpg);
			} catch (IOException e) {
				throw new IllegalArgumentException("exists" + f.exists()
					+ ", direc" + f.isDirectory(), e);
			}
		}
		File[] arr = f.listFiles(jpg);
		MediaTracker t = new MediaTracker(require);
		BufferedImage[] result = new BufferedImage[arr.length];
		for (int i = 0; i < arr.length; i++) {
			try {
				result[i] = ImageIO.read(arr[i]);
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			t.addImage(result[i], i);
		}
		boolean fail = false;
		try {
			t.waitForAll();
		} catch (InterruptedException e) {
			fail = true;
		}
		if (t.isErrorAny() || fail)
			throw new RuntimeException();
		return result;
	}
	private static BufferedImage[] doJarIMG(String loc, FilenameFilter ff) throws IOException{
		CodeSource src = Util.class.getProtectionDomain().getCodeSource();
		//ArrayList<String> list = new ArrayList<String>();
		ArrayList<BufferedImage> list1 = new ArrayList<BufferedImage>();
		if( src != null ) {
			ZipInputStream zip = null;
			try {
				URL jar = src.getLocation();
				zip = new ZipInputStream(jar.openStream());
				ZipEntry ze = null;

				while ((ze = zip.getNextEntry()) != null) {
					String name = ze.getName();
					if (ff.accept(null, name)) {
						if (name.startsWith(loc)) {
							list1.add(ImageIO.read(zip));
							// list.add( name );
							// System.out.println(name);
						}
					}
				}
			} finally {
				if (zip != null)
					zip.close();
			}
		 }
		System.out.println("Size = " + list1.size());
		//final int si = list.size();
		BufferedImage[] result = new BufferedImage[list1.size()];
		list1.toArray(result);
		//for(int i = 0; i < si; i++){
		//	result[i] = ImageIO.read(System.class.getResourceAsStream(list.get(i)));
		//}
		return result;
		 //webimages = list.toArray( new String[ list.size() ] );
	}
	

	
	
	public static InputStream getJarStream(String filename) throws IOException{

		CodeSource src = Util.class.getProtectionDomain().getCodeSource();
		if( src != null ) {
			ZipInputStream zip = null;
		    URL jar = src.getLocation();
		    zip = new ZipInputStream( jar.openStream());
		    ZipEntry ze = null;
		    
		    while((ze = zip.getNextEntry() ) != null ) {
		    	if(ze.getName().equals(filename)){
		    		return zip;
		    		//PipedOutputStream po = new PipedOutputStream();
		    		//ze.getSize();
		    		//po.
		    	}
		    }
		    throw new FileNotFoundException(filename);
	    }
		throw new UnsupportedOperationException("Not a jar");
	}
	public static void printThreads(){
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
		StringBuilder sb = new StringBuilder().append(threadArray.length).append(" REMAINING THREADS:{\n");
		for(int i = 0; i < threadArray.length-1; i++){
			sb.append(threadArray[i]).append(",\n");
		}
		sb.append(threadArray[threadArray.length-1]).append("}");
		System.out.println(sb);
	}

	public static String[] getConnections(){
		ArrayList<String> sb = new ArrayList<String>();
		try {
			Enumeration<NetworkInterface> e;
			e = NetworkInterface.getNetworkInterfaces();
			while (e.hasMoreElements()) {
				Enumeration<InetAddress> ee = e.nextElement()
						.getInetAddresses();
				while (ee.hasMoreElements()) {
					String i = ee.nextElement().getHostAddress();
					if (i.indexOf('.') != -1 && !i.startsWith("127"))
							sb.add(i);
				}
			}	
		} catch (SocketException e1) {
			System.out.println(e1);
		}
		return sb.toArray(new String[sb.size()]);
	}

	public static void setDefaultLAF() {
		try{
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception e){
			logW(e.toString());
		}
	}
	
	/** 
	 * Sun property pointing the main class and its arguments. 
	 * Might not be defined on non Hotspot VM implementations.
	 */
	public static final String SUN_JAVA_COMMAND = "sun.java.command";

	/**
	 * Restart the current Java application
	 * @param runBeforeRestart some custom code to be run before restarting
	 * @throws IOException
	 */
	public static void restartApplication(Runnable runBeforeRestart) throws IOException {
		try {
			// java binary
			String java = System.getProperty("java.home") + "/bin/java";
			// vm arguments
			List<String> vmArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
			StringBuffer vmArgsOneLine = new StringBuffer();
			for (String arg : vmArguments) {
				// if it's the agent argument : we ignore it otherwise the
				// address of the old application and the new one will be in conflict
				if (!arg.contains("-agentlib")) {
					vmArgsOneLine.append(arg);
					vmArgsOneLine.append(" ");
				}
			}
			// init the command to execute, add the vm args
			final StringBuffer cmd = new StringBuffer("\"" + java + "\" " + vmArgsOneLine);

			// program main and program arguments
			String[] mainCommand = System.getProperty(SUN_JAVA_COMMAND).split(" ");
			// program main is a jar
			if (mainCommand[0].endsWith(".jar")) {
				// if it's a jar, add -jar mainJar
				cmd.append("-jar " + new File(mainCommand[0]).getPath());
			} else {
				// else it's a .class, add the classpath and mainClass
				cmd.append("-cp \"" + System.getProperty("java.class.path") + "\" " + mainCommand[0]);
			}
			// finally add program arguments
			for (int i = 1; i < mainCommand.length; i++) {
				cmd.append(" ");
				cmd.append(mainCommand[i]);
			}
			// execute the command in a shutdown hook, to be sure that all the
			// resources have been disposed before restarting the application
			Runtime.getRuntime().addShutdownHook(new Thread() {
				@Override
				public void run() {
					try {
						Runtime.getRuntime().exec(cmd.toString());
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			});
			// execute some custom code before restarting
			if (runBeforeRestart!= null) {
				runBeforeRestart.run();
			}
			// exit
			System.exit(0);
		} catch (Exception e) {
			// something went wrong
			throw new IOException("Error while trying to restart the application", e);
		}
	}

}
