package org.ace.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javazoom.jl.player.Player;

/**
 * JayLayer
 * 
 * JayLayer is a library based off the JLayer library, to enable Java noobs to use it.
 * 
 * @author Jay Kamat
 * @version 2.1
 * Manages Playlists for MP3 files
 * 
 * A very tiny bit of this was made by shaines...thank you! (I did mod his part though)
 * 
 * Released under the Lesser GNU Lesser General Public License V3.0
 * Read about it online please cause I don't want to paste it here
 * 
 * This program uses an unmodified version of the JLayer library (1.0.1), it is just bundled inside for convenience.
 * Find the unmodified JLayer library at <http://www.javazoom.net/javalayer/javalayer.html>
 * 
 * 
 * Copyright Jay Kamat, 2013
 */
public class JayLayer {
	private MP3Player sound;
	private String currentSongName;
	//two "types" of sound... titlesounds and gamesounds (there is a big difference)
	private ArrayList<ArrayList<String>> playLists;
	private ArrayList<MP3Player> soundEffects;
	private int currentPlayList=-1;
	private int currentSong=-1;
	private Random ran;
	private boolean dSoundCheck;
	private String prefixBackgroundEffects; //no prefix is inside the src folder (default package)\
	private String prefixSoundEffects;
	private long pause;

	/**
	 * 
	 * @param prefixBackgroundSounds a prefix (or path) to sound files for background sounds. This is the src directory if empty. Example: "/audio/"
	 * @param doubleSoundCheck if true, this will do a check so a song wont play twice in a row. Leave false if only 1 song in any playlist, otherwise, leave true
	 * @param prefixSoundEffects a prefix (or path) to sound files for effects. This is the src directory if empty. Example: "/audio/"
	 * 
	 */
	public JayLayer(String prefixBackgroundSounds,String prefixSoundEffects, boolean doubleSoundCheck){
		super();
		this.dSoundCheck=doubleSoundCheck;
		playLists=new ArrayList<ArrayList<String>>();
		soundEffects=new ArrayList<MP3Player>();
		this.prefixBackgroundEffects=prefixBackgroundSounds;
		this.prefixSoundEffects=prefixSoundEffects;
		ran=new Random();
		pause=0;
	}
	
	/**
	 * This constructor assumes all sound files are in the src directory
	 * @param doubleSoundCheck if true, this will do a check so a song wont play twice in a row. Leave false if only 1 song in any playlist, otherwise, leave true
	 */
	public JayLayer(boolean doubleSoundCheck){
		super();
		this.dSoundCheck=doubleSoundCheck;
		playLists=new ArrayList<ArrayList<String>>();
		soundEffects=new ArrayList<MP3Player>();
		this.prefixBackgroundEffects=null;
		this.prefixSoundEffects=null;
		ran=new Random();
		pause=0;
	}

	/**
	 * Adds a playlist
	 * @return the index of the playlist you added
	 */
	public int addPlayList(){
		playLists.add(new ArrayList<String>());
		return playLists.size()-1;
	}

	/**
	 * Adds a song to a playlist
	 * @param theList the playlist to add the song to
	 * @param song the filename of the song (the prefix will be included)
	 * @return the index of the song you just added (if you need it)
	 */
	public int addSong(int theList, String song){
		if(theList>=playLists.size())
			throw new IllegalArgumentException("That playlist dosent exist!");
		playLists.get(theList).add(prefixBackgroundEffects+""+song);
		return playLists.get(theList).size()-1;
	}

	/**
	 * Adds a sound to the soundEffects playlist
	 * @param song
	 * @return The sound effect handle.
	 */
	public int addSoundEffect(String song){
		this.soundEffects.add(new MP3Player(prefixSoundEffects+""+song,this,false));
		return soundEffects.size()-1;
	}
	
	/**
	 * Plays a sound effect
	 * @param i the index of the sound effect to play
	 */
	public void playSoundEffect(int i){
		soundEffects.get(i).play();
	}

	/**
	 * Adds songs to a playlist from an arragy
	 * @param theList the playlist to add songs to
	 * @param songs the array of songs
	 */
	public void addSongs(int theList, String[] songs){
		if(theList>=playLists.size())
			throw new IllegalArgumentException("That playlist dosent exist!");
		for(String in:songs){
			playLists.get(theList).add(prefixBackgroundEffects+""+in);
		}
	}

	/**
	 * Adds songs to a playlist from a list (you can use an arraylist here)
	 * @param theList the playlist to add to
	 * @param songs the songs to add
	 */
	public void addSongs(int theList, List<String> songs){
		if(theList>=playLists.size())
			throw new IllegalArgumentException("That playlist dosent exist!");
		for(String in:songs){
			playLists.get(theList).add(prefixBackgroundEffects+""+in);
		}
	}

	/**
	 * Sets the soundChecker (see constructor)
	 * @param b what to set it to
	 */
	public void setDoubleSoundCheck(boolean b){
		this.dSoundCheck=b;
	}

	/**
	 * Gets the soundChecker (see constructor)
	 * @return the value of the soundCheck
	 */
	public boolean getDoubleSoundCheck(){
		return this.dSoundCheck;
	}

	/**
	 * Changes the playlist to your liking. This also starts sound if there is none playing
	 * @param i the playlist to change to
	 */
	public void changePlayList(int i){
		if(i>=playLists.size())
			throw new IllegalArgumentException("That playlist dosent exist!");
		currentPlayList=i;
		this.changeSound();
	}

	private void changeSound(int playList){
		//cycles throught the playlist, randomly except if title is the same as the last title
		String old = currentSongName;
		currentPlayList = playList; // letting the rest of the class know
		currentSong = ran.nextInt(playLists.get(currentPlayList).size());
		currentSongName = playLists.get(currentPlayList).get(currentSong);
		if (old != null && old.equals(currentSongName) && this.dSoundCheck)
			changeSound(currentPlayList);
	}


	private void startSound(){
		if(pause!=0){
			try {
				Thread.sleep(pause);
			} catch (InterruptedException e) {
			}
		}
		changeSound(currentPlayList);
		sound=new MP3Player(currentSongName,this,true);
		sound.play();
	}

	private void stopSound(){
		if(sound!=null&&sound.player!=null)
			sound.player.close();
	}

	/**
	 * changes the sound (stays in the same playlist). Please call the setPlaylist method first
	 * this also starts the sound if there is nothing playing
	 */
	public void changeSound(){
		if(playLists.isEmpty())
			throw new IllegalArgumentException("You havent added any playlists yet!");
		if(currentPlayList==-1)
			throw new IllegalArgumentException("You havent set the playlist to play!");
		if(playLists.get(currentPlayList).isEmpty()){
			throw new IllegalArgumentException("This playlist dosent have any songs!");
		}
		stopSound();
		startSound();
	}

	/**
	 * gets the amount of time to wait between songs (in milliseconds)
	 * @return The wait time
	 */
	public long getWait() {
		return pause;
	}

	/**
	 * Sets the amount of time to wait
	 * @param pause the amount of time to wait (in milliseconds)
	 */
	public void setWait(long pause) {
		this.pause = pause;
	}
	
	
	/**
	 *
	 * @author shaines
	 * 
	 * modified for use by this library by Jay Kamat
	 * 
	 * For a user of this library, this is the part that makes the sound work. Dont use it. If you want to use this
	 * Directly, instead just use the JLayer library. Using this makes this library pointless.
	 */
	private class MP3Player {

	    private Player player;
	    private InputStream is;
	    private JayLayer top;
	    private boolean repeat;
	    private String name;

	    public MP3Player(String filename,JayLayer t, boolean repeat) {
	    	name=filename;
	        top=t;
	        this.repeat=repeat;
	        try {
	            //
	        	//File f = new java.io.File(filename).getAbsoluteFile();
	        	//System.out.println(f.getAbsolutePath());
	        	is=new FileInputStream(filename);
	        } catch (Exception e) {
	        	try{
	        		is = Util.getJarStream(filename);
	        		return;
	        	}catch(Exception e2){
	        		e2.printStackTrace();
	        	}
	            e.printStackTrace();
	            throw new RuntimeException(e);
	        }
	    }

	    public void play() {
	        try {
	            player = new Player(is);
	            PlayerThread pt = new PlayerThread(name);
	            pt.start();
	        } catch (Exception e) {
	            e.printStackTrace();
	            throw new RuntimeException(e);
	        }
	    }

	    class PlayerThread extends Thread {

	    	public PlayerThread(String name){
	    		super("Sound Thread, playing: "+name);
	    	}
	    	
	        public void run() {
	            try {
	                player.play();
	                if(repeat&&player.isComplete())
	                    top.changeSound();
	            } catch (Exception e) {
	                e.printStackTrace();
	                throw new RuntimeException(e);
	            }
	        }
	    }
	}
	
}


