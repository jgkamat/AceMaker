package org.ace.entity;

import java.util.ArrayList;

import org.ace.phys.Vector2D;
import org.ace.util.Settings;



/**
 * Turrets.attack();
 * Probably not going to be working for the presentation
 * @author Jay Kamat
 * @version 0.02
 *
 */
public class TurretEntity extends Entity implements TeamFighter{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7251031645938819600L;
	private ArrayList<TeamFighter> targets=new ArrayList<TeamFighter>();
	private TeamFighter target;
	private EntityManager aggregate;
	private short teamNum;

	public TurretEntity(double x, double y, double width, double height,EntityManager itsAnAggeregate) {
		super(x, y, width, height);
		this.aggregate=itsAnAggeregate;
		teamNum=0;
	}

	public void givePotentialTargets(ArrayList<TeamFighter> targ){
		targets=targ;
	}

	public void giveTarget(TeamFighter targ){
		target=targ;
	}

	public void selectTarget(){
		double minDis=Double.MAX_VALUE;
		for(TeamFighter imGonnaGetYou:targets){
			if(imGonnaGetYou.isFriendly(this)&&Vector2D.getDistance(getX(), this.getY(), imGonnaGetYou.getX(), imGonnaGetYou.getY())<minDis){
				target=imGonnaGetYou;
				minDis=Vector2D.getDistance(getX(), this.getY(), imGonnaGetYou.getX(), imGonnaGetYou.getY());
			}

		}
	}

	public void act(){
		if(target!=null&&target instanceof Entity){
			aggregate.addProjectile(new HomingEntity(this.getX(),this.getY()-this.getHeight()-5, Settings.bulletMass,(Entity) this.target, 
					Vector2D.getDirection(this.getX(), this.getY(), target.getX(), target.getY())));
		} else
			throw new IllegalArgumentException("Target isnt working right");

	}

	@Override
	public short getTeam() {
		return teamNum;
	}

	@Override
	public boolean isEnemy(TeamFighter other) {
		if(this.teamNum==0)
			return true;
		if(other.getTeam()!=this.getTeam()){
			return true;
		}
		return false;
	}

	@Override
	public boolean isFriendly(TeamFighter other) {
		return !isEnemy(other);
	}

	@Override
	public void setTeam(short team) {
		this.teamNum=team;

	}


}
