package org.ace.entity;


public class MovingEntity extends Entity{
	
	/**
	 * Moving is good, isnt it
	 */
	private static final long serialVersionUID = 6396935402789415484L;

	private transient double mass;
	
	/**
	 * A Entity that can now move! Fancy!
	 * @param x
	 * @param y
	 * @param w Width
	 * @param h Height
	 * @param mass MASS IS NOW AVAIALABLE! I wasnt sure where exactally to add this, so here it is
	 */
	public MovingEntity(double x, double y, double w, double h, double mass){
		super(x,y,w,h);
		this.mass=mass;
	}
	
	public double getMass(){
		return mass;
	}
	
	public void moveTo(double x, double y) {
		this.setX(x);
		this.setY(y);
	}
	
	public void move(double x, double y) {
		this.setX(this.getX()+x);
		this.setY(this.getY()+y);
	}
	
	public boolean equals(MovingEntity other){
		return super.equals(other);
	}
	
}

