package org.ace.entity;

import org.ace.phys.Vector2D;

/**
 * A class that represents a entity that will (hopefully) home
 * @author Jay Kamat
 * @version 2013.5.19
 *
 */
public class HomingEntity extends MovingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8910356126897575637L;
	private transient Entity target;
	private static final double speed=20,maxRotation=0.1;
	private transient Vector2D vel;
	public static transient short damage=100;


	/**
	 * Dont ask me why a constructor is needed
	 * @param x
	 * @param y
	 * @param mass
	 * @param target 
	 * @param initialDir so things dont look wierd?
	 */
	public HomingEntity(double x, double y, double mass, Entity target,double initialDir) {
		super(x, y, 20, 10, mass);
		this.target=target;
		vel=new Vector2D(this.getX(),this.getY(),initialDir,speed,false);
	}

	private HomingEntity(double x, double y, double w, double h, double mass, Entity target,double initialDir) {
		super(x, y, w, h, mass);
		this.target=target;
		vel=new Vector2D(this.getX(),this.getY(),initialDir,speed,false);
	}

	public void setTarget(Entity replacement){
		this.target=replacement;
	}

	public Entity getTarget(){
		return target;
	}

	/**
	 * makes directions so nothing is above pi(2) and below 0
	 */
	private void fixDir(){
		if(vel.getDirection()<0)
			vel.setDirection(vel.getDirection()+2*Math.PI);
		if(vel.getDirection()>2*Math.PI)
			vel.setDirection(vel.getDirection()-2*Math.PI);
	}

	/**
	 * Hopefully, nice moving tracking things will happen here
	 * 
	 * 
	 */
	public void act(){
		if(target!=null){
			fixDir();
			double directionToTurn=Vector2D.getDirection(this.getX(), this.getY(), target.getX(), target.getY());
			double direction=vel.getDirection();

			if(Math.abs(direction-directionToTurn)<HomingEntity.maxRotation){
				vel.setDirection(directionToTurn);
			}
			else{
				if(direction-directionToTurn<Math.PI&&direction-directionToTurn>0){
					vel.setDirection(vel.getDirection()-HomingEntity.maxRotation);
				}
				else if(direction-directionToTurn>Math.PI&&direction-directionToTurn>0){
					vel.setDirection(vel.getDirection()+HomingEntity.maxRotation);
				}
				if(direction-directionToTurn<0&&(direction-directionToTurn+Math.PI*2<Math.PI)){
					vel.setDirection(vel.getDirection()-HomingEntity.maxRotation);
				}
				else if(direction-directionToTurn<0&&(direction-directionToTurn+Math.PI*2>Math.PI)){
					vel.setDirection(vel.getDirection()+HomingEntity.maxRotation);
				}
			}

			this.setX(vel.getX2());
			this.setY(vel.getY2());
			vel=(new Vector2D(vel.getX2(),vel.getY2(),vel.getX2()+vel.getX(),vel.getY2()+vel.getY()));

			this.setRotation(vel.getDirection());
		}
	}


}
