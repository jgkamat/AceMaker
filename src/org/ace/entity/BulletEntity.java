package org.ace.entity;

import org.ace.util.Settings;
import org.ace.phys.Vector2D;

/**
 * Class to represent a bullet. 
 * Calling the act method should make the bullet move forward one "unit"
 * Gravity will be there if gravityWorking is called
 * @author Jay Kamat
 * @version 2013.5.26
 *
 */
public class BulletEntity extends MovingEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6788491154766217759L;
	private transient Vector2D gravity, vel;
	private static transient boolean gravityWorking=false;
	public static transient short damage=5;
	//TODO set to true if it looks better with gravity

	/**
	 * Velocity x1 and y1 should be the same as x and y that you dump in here
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param mass
	 * @param vel The initial velocity of the projecitle. Ususally you only need to set it here
	 */
	public BulletEntity(double x, double y, double w, double h, double mass,Vector2D vel) {
		super(x, y, w, h, mass);
		this.vel=vel;
	}

	/**
	 * Vector of the initial velocity of the projectile. Designed for bullets
	 * The vector MUST Have the same x1 and y1 as the projectile itself
	 * @param in The vector that goes in
	 */
	public void setVel(Vector2D in){
		vel=in;
	}
	
	/**
	 * makes the bullet go where it needs to!
	 */
	public void act(){
		if(gravityWorking){
			gravity=new Vector2D(this.getX(),this.getY(),this.getX(),this.getY()-1,Settings.getG());
			
			//Gravity does not need mass calculations as it is made with the acceleration g
			vel.resultant(gravity);
		}

		this.setX(vel.getX2());
		this.setY(vel.getY2());

		vel=new Vector2D(vel.getX2(),vel.getY2(),vel.getDirection(),vel.getLength(),true);
	}
}
