package org.ace.entity;

/**
 * I used an interface. I will never forgive myself.
 * @author Jay Kamat
 * 
 * 
 * Anyway, it represents a guy who is loyal (or not) to a team!
 *
 */
public interface TeamFighter {
	
	/**
	 * You can javadoc interfaces too! How useless.
	 * @return I wonder what it would return
	 */
	public short getTeam();

	public boolean isEnemy(TeamFighter other);

	public boolean isFriendly(TeamFighter other);

	public void setTeam(short team);

	/**
	 * Bascially here because of aggregates.
	 * @return
	 */
	public double getX();
	public double getY();
	
}
