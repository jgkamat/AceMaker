package org.ace.entity;

import org.ace.util.Settings;
import org.ace.phys.Vector2D;

/**
 * FLYING STUFF
 * Anything that can fly (any player) extends this
 * Also contains some "player" stuff, like teams, health, ect.
 * @author Jay Kamat
 * @version 2013.26.22
 *
 */
public class FlyingEntity extends MovingEntity implements TeamFighter{

	private static final long serialVersionUID = -4249825519833884449L;
	/**
	 * 
	 */
	private transient Vector2D thrust,drag,gravity,lift,vel;
	private transient double maxThrust,percentOfThrust=0.0,aircraftDrag,liftFactor, rotationChangeValue;
	private volatile boolean firingBullets,missileLaunch;
	private volatile short health, teamNum;
	public static transient final double autoTurnAmt=0.1; 
	private transient short missileCount=3;



	/**
	 * Rotation, flying, ect is all added here
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 * @param mass
	 * @param maxThrust
	 * @param airDrag
	 * @param liftFactor
	 */
	public FlyingEntity(double x, double y, double w, double h,double mass,double maxThrust,double airDrag,double liftFactor) {
		super(x, y, w, h,mass);
		thrust=new Vector2D();
		drag=new Vector2D();
		gravity=new Vector2D();
		lift=new Vector2D();
		vel=new Vector2D(this.getX(),this.getY(),this.getX(),this.getY());
		this.maxThrust=maxThrust;
		this.aircraftDrag=airDrag;
		this.liftFactor=liftFactor;
		this.teamNum=0;
		this.firingBullets=false;
		this.missileLaunch=false;
		health=1000;
	}

	public void setThrust(double percent){
		this.percentOfThrust=percent;
	}

	public void setRotationChangeOnAct(double radians){
		rotationChangeValue = radians;
	}

	/**
	 * Gets the rotation in RADIANS
	 * @return Rotation. RADIANS
	 */


	public void turnClockwise(double dir){
		this.setRotation(this.getRotation()-dir);
	}

	public void turnCounterClockwise(double dir){
		this.setRotation(this.getRotation()+dir);
	}

	private void fixDir(){
		if(this.getRotation()<0){
			this.setRotation(this.getRotation()+Math.PI*2);
		}
		else if(this.getRotation()>=Math.PI*2){
			this.setRotation(this.getRotation()-Math.PI*2);
		}
	}

	/**
	 * Creates a bulletentity that looks like its being "shot" from the muzzle
	 * @return
	 */
	public BulletEntity spawnBullet(){
		Vector2D temp=new Vector2D(this.getX(),this.getY(),this.getRotation(),this.getWidth()/2.0,true);
		Vector2D awsomeVel=new Vector2D(temp.getX2(),temp.getY2(),temp.getDirection()+(Math.random()*0.1745329252-0.0872664626),20,true);
		awsomeVel.resultant(this.vel);
		return new BulletEntity(temp.getX2(),temp.getY2(),2,2,10,awsomeVel);
	}

	public boolean isFiringBullets(){
		return firingBullets;
	}

	public void setBulletFire(boolean b){
		if(health>0)
			firingBullets=b;
	}


	/**
	 * Makes the plane move, in short
	 * All of the physics goes into this method in some form
	 * Without this, everything wouldnt move (could be helpful *hint hint*)
	 */
	public void act(){
		if(this.health>0){
			
			this.loopCheck();
			
			fixDir();
			gravity=new Vector2D(this.getX(),this.getY(),this.getX(),this.getY()+1,Settings.getG());
			thrust=new Vector2D(this.getX(),this.getY(),this.getRotation(),maxThrust*this.percentOfThrust,false);
			drag=new Vector2D(this.getX(),this.getY(),vel.getX1()-vel.getX(),vel.getY1()-vel.getY(),0.5*
					Math.pow(vel.getLength(),2)*Settings.getAirDensity()*this.aircraftDrag);

			double liftFactor2=0;
			double angleOfAttack=(this.getRotation()-vel.getDirection());

			if(angleOfAttack>Math.PI)
				angleOfAttack-=Math.PI*2;

			if(this.getRotation()>Math.PI/2&&this.getRotation()<Math.PI*3/2){
				angleOfAttack=-angleOfAttack;
			}

			//System.out.println(angleOfAttack);
			if(angleOfAttack>-.7853&&angleOfAttack<0.17453){
				liftFactor2=1.410-8.261*angleOfAttack+2.809*Math.pow(angleOfAttack, 2)+1.805*Math.pow(angleOfAttack, 3)
						-9.298*Math.pow(angleOfAttack, 4)+18.35*Math.pow(angleOfAttack, 5);
			}

			double liftdir=this.getRotation()+Math.PI/2;
			if(liftdir-Math.PI/2>Math.PI/2&&liftdir-Math.PI/2<3*Math.PI/2){
				liftdir=liftdir+Math.PI;
			}

			double liftamt=(Math.pow(vel.getLength(),2))*
					(Math.cos(vel.getDirection()-this.getRotation()));
			//System.out.println(liftFactor2);
			if(liftamt<0)
				liftamt=0;

			lift=new Vector2D(this.getX(),this.getY(),liftdir,-(liftamt)*liftFactor*liftFactor2,false);

			if(this.getY()>200)
				vel.resultant(thrust.setDistance(thrust.getLength()/getMass()));
			vel.resultant(lift.setDistance(lift.getLength()/getMass()));
			vel.resultant(drag.setDistance(drag.getLength()/getMass()));

			//gravity is always the same, so mass is not needed for its calculations
			vel.resultant(gravity);

			this.setX(vel.getX2());
			this.setY(vel.getY2());
			vel=(new Vector2D(vel.getX2(),vel.getY2(),vel.getX2()+vel.getX(),vel.getY2()+vel.getY()));

			autoChangeRot();
		}
		else{
			this.setBulletFire(false);
		}
		/*
		 * And here we go again...
		 * 
		 * If you dont know how this method works, heres how in some simple steps
		 * 1. fix direction. This makes rotation values to be within 0--> 2pi, nothing important
		 * 2. calculate vectors for every acceleration.
		 * 2a. gravity is a constant acceleration. Nothing fancy here
		 * 2b. thurst is in the directon of "rotation" and can be set
		 * 2c. drag is opposite the direction of flight. varies with velocity
		 * 3d. Lift is complicated. Direction is +90deg to rotation, and varies VERY WEIRDLY with the AIRSPEED not groundspeed
		 * 3d1 (new):Lift just got more complicated! It increases with angle of attack (veldir-dir) and decreases when a stall occurs.
		 * 3d2 (new):You can find the lift equation in the code above. Not compeltely realistic, I made it myself 
		 * 3d3 (new): life that I could do without actually doing airflow calculations and stuff. 
		 * 4. add up those vectors
		 * 5. move that plane to the end of the vector
		 * 6. move the acceleration vector so the end is at the location of the guy. sort of like inertia
		 * 
		 * NOTES: from pi/2-->3pi/2 the aircraft behaves like it has been flipped. We should flip the image to reflect this.
		 * 
		 * And thats it! If your confused go learn some more physics.
		 */
	}

	
	private void loopCheck(){
		if(this.getX()<0){
			this.setX(Settings.getWidth()-1);
		}
		else if(this.getX()>Settings.getWidth())
			this.setX(1);
		vel=new Vector2D(this.getX(),this.getY(),this.getX()+vel.getX(),this.getY()+vel.getY());
	}

	/**
	 * HA HA HA THIS IS PRIVATE. I DONT NEED TO JAVADOC THIS
	 */
	private void autoChangeRot(){

		double turnFactor=Math.abs(vel.getDirection()-this.getRotation());
		if(turnFactor>Math.PI*3/2){
			turnFactor=Math.abs(turnFactor-Math.PI*2);
		}
		else if(turnFactor>Math.PI)
			turnFactor-=Math.PI;

		//if(Math.abs(vel.getDirection()-this.getRot())>0.349&&Math.abs(vel.getDirection()-this.getRot())<5.9341||
		//		rotationChangeValue==0&&Math.abs(vel.getDirection()-this.getRot())>0.087266&&
		//		Math.abs(vel.getDirection()-this.getRot())<6.195918){
		if(this.getRotation()-vel.getDirection()<Math.PI&&this.getRotation()-vel.getDirection()>0){
			setRotation(this.getRotation()-FlyingEntity.autoTurnAmt*(vel.getLength()/getMass())*turnFactor);
		}
		else if(this.getRotation()-vel.getDirection()>Math.PI&&this.getRotation()-vel.getDirection()>0){
			setRotation(this.getRotation()+FlyingEntity.autoTurnAmt*(vel.getLength()/getMass())*turnFactor);
		}
		if(this.getRotation()-vel.getDirection()<0&&(this.getRotation()-vel.getDirection()+Math.PI*2<Math.PI)){
			setRotation(this.getRotation()-FlyingEntity.autoTurnAmt*(vel.getLength()/getMass())*turnFactor);
		}
		else if(this.getRotation()-vel.getDirection()<0&&(this.getRotation()-vel.getDirection()+Math.PI*2>Math.PI)){
			setRotation(this.getRotation()+FlyingEntity.autoTurnAmt*(vel.getLength()/getMass())*turnFactor);
		}
		//}

		double temp=(Math.cos(vel.getDirection()-this.getRotation()));
		if(temp<0)
			temp=0;
		this.setRotation(getRotation() + rotationChangeValue*(vel.getLength()/getMass())*temp);
	}
	public String toString(){
		return super.toString() + ", rot =" + this.getRotation();
	}

	/**
	 * Equality (movment)
	 * @param other
	 * @return
	 */
	public boolean equals(FlyingEntity other){
		return other.firingBullets == firingBullets && super.equals(other);
	}

	public Vector2D getVel(){
		return vel;
	}

	public void setVel(Vector2D verocity){
		this.vel=verocity;
	}

	public short getHealth() {
		return health;
	}

	public void setHealth(short health) {
		this.health = health;
	}

	public void takeDamage(short howMuch){
		if(health-howMuch<=0)
			health=0;
		else
			health-=howMuch;
	}


	@Override
	public short getTeam() {
		return teamNum;
	}

	@Override
	/**
	 * AKA: is it a south korean?
	 */
	public boolean isEnemy(TeamFighter other) {
		if(this.teamNum==0)
			return true;
		if(other.getTeam()!=this.getTeam()){
			return true;
		}
		return false;
	}

	@Override
	public boolean isFriendly(TeamFighter other) {
		return !isEnemy(other);
	}

	/**
	 * If you are a spy, this is a good place to start
	 */
	public void setTeam(short team) {
		this.teamNum= team;

	}

	public short getMissileCount() {
		return missileCount;
	}

	public void setMissileCount(short missileCount) {
		this.missileCount = missileCount;
	}

	public boolean isMissileLaunching() {
		return missileLaunch;
	}

	public void setMissileLaunch(boolean missile) {
		this.missileLaunch = missile;
	}


}
