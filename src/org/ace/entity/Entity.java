package org.ace.entity;

import java.io.Serializable;

import org.ace.phys.Vector2D;

/**
 * Anything in the game extends this class. Its a simple class to hold coordinates and dimensions
 * ANY COORDINATES IN THIS GAME ARE THE CENTER OF THE OBJECT. They technically dont have to be though...
 * @author Jay Kamat
 * @version 2013.5.19
 *
 */
public class Entity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8545412293241303213L;
	private volatile double x, y, width, height, rotation;
	private transient boolean visible=true;
	//private final int entityId;
	//public int updateNumber;
	//private static int counter = Integer.MIN_VALUE;

	/**
	 * Represents a basic object in the game, that dosent move
	 * @param x 
	 * @param y
	 * @param width
	 * @param height
	 */
	public Entity(double x, double y,double width,double height){
		this.x=x;
		this.y=y;
		this.width=width;
		this.height=height;
		this.rotation = 0.0;
		//this.entityId= counter++;
	}

	public boolean isVisible(){
		return visible;
	}

	public void setVisiblity(boolean b){
		visible=b;
	}
	
	public double getRotation(){
		return rotation;
	}
	
	public void setRotation(double rot){
		rotation=rot;
	}

	public double getX(){
		return x;
	}

	public double getY(){
		return y;
	}

	public void setX(double x){
		this.x=x;
	}

	public void setY(double y){
		this.y=y;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	//public int getID(){
	//	return entityId;
	//}


	/**
	 * Simple Collision detection
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isPointInside(double x, double y) {
		this.x-=this.width/2;
		this.y-=this.height/2;
		if (x >= this.x && x <= this.x + width && y >= this.y && y <= this.y + height){
			this.x+=this.width/2;
			this.y+=this.height/2;
			return true;
		}
		this.x+=this.width/2;
		this.y+=this.height/2;
		return false;
	}
	
	/**
	 * Really helpful collision stuff
	 * @param other
	 * @return
	 */
	public boolean isInside(Entity other){
		//this random stuff is because the point (x,y) is the CENTER point
		//not the corner...
		this.x-=this.width/2;
		this.y-=this.height/2;
		other.x-=other.width/2;
		other.y-=other.height/2;
		if(x<other.x+other.width&&x+width>other.x&&y<other.y+other.height&&y+height>other.y){
			this.x+=this.width/2;
			this.y+=this.height/2;
			other.x+=other.width/2;
			other.y+=other.height/2;
			return true;
		}
		this.x+=this.width/2;
		this.y+=this.height/2;
		other.x+=other.width/2;
		other.y+=other.height/2;
		return false;

	}
	
	/**
	 * Steals a method from Vector2D. Makes simple stuff
	 * @param other
	 * @return
	 */
	public double distanceTo(Entity other){
		return Vector2D.getDistance(this.getX(), this.getY(), other.getX(), other.getY());
	}
	public void act(){
		
	}
	
	public String toString(){
		return this.getClass().getName() + "x =" + Math.round(x) + ", y =" + (int)Math.round(y);
	}

	/**
	 * Only provides a few simple checks to see if somthing is equal.
	 * @param other
	 * @return equality.
	 */
	public boolean equals(Entity other) {
		return (other.x == x) && (other.y == y) && (other.width == width)
				&& (other.height == height) && (other.rotation == rotation);
	}


}
