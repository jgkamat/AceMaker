package org.ace.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.ace.network.NetworkServer;
import org.ace.network.ServerToClientConnection;
import org.ace.phys.Vector2D;
import org.ace.util.Settings;

/**
 * A Class for managing and distributing entities
 * I learned my lesson from passing around an array in JPlanetSim
 * This class holds planes, projectiles, and stationary stuff (obstacles[not implemented]). 
 * It is a property of the server
 * @author  Jay
 * @version 2013.5.21
 */
public class EntityManager implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4707410894383262730L;
	private ArrayList<Entity> stationary;
	//private ArrayList<FlyingEntity> flying; 


	//this array is here only for the locations of the objects...It does not keep inertia or whatever updated
	//this array CANNOT be used to act the planes, only the clients can do that.
	private volatile List<ServerToClientConnection> flying;

	private List<MovingEntity> projectile;
	public static final double lookingThreshold=1000; //close enough to 1080 
	private NetworkServer ns;
	private Thread entityActor;
	//private long sleepTime = 1000 / 60;
	//private AtomicBoolean isDirty;

	/**
	 * A class to hold and manage entities
	 */
	public EntityManager(NetworkServer s){
		ns = s;
		stationary=new ArrayList<Entity>();
		flying=ns.getConnections();
		projectile=Collections.synchronizedList(new ArrayList<MovingEntity>());
		//isDirty = new AtomicBoolean(false);
		/*entityActor = new Thread(new Runnable(){

			@Override
			public void run() {
				try{
					while(!entityActor.isInterrupted()){
						long startTime = System.currentTimeMillis();
						act();
						long st = sleepTime - (System.currentTimeMillis() - startTime);
						if(st > 0)
							Thread.sleep(st);
						else
							Thread.yield();
					}
				}catch(InterruptedException ie){

				}
			}}, "EntityActor for Manager" + this);
		entityActor.setDaemon(true);
		entityActor.start();*/
	}

	/**
	 * Gets stuff around a certain entity
	 * @param toLookWith 
	 * @return an arraylist of entities
	 */
	public ArrayList<Entity> getAround(Entity toLookWith){
		ArrayList<Entity> out=new ArrayList<Entity>();
		for(Entity find:stationary){
			if(toLookWith.distanceTo(find)<=lookingThreshold){
				if(find!=toLookWith)
					out.add(find);
			}
		}
		for(ServerToClientConnection find:flying){

			if(toLookWith.distanceTo(find.getEntity())<=lookingThreshold){
				if(find.getEntity()!=toLookWith&&find.getEntity().getHealth()>0)
					out.add(find.getEntity());
			}
		}
		synchronized(projectile){
			for(Entity find:projectile){
				if(toLookWith.distanceTo(find)<=lookingThreshold){
					if(find!=toLookWith)
						out.add(find);
				}
			}
		}
		return out;
	}

	public void addStationary(Entity add){
		stationary.add(add);
	}

	public void addProjectile(MovingEntity add){
		projectile.add(add);
	}


	public ArrayList<Entity> getStationary() {
		return stationary;
	}


	public List<ServerToClientConnection> getClients() {
		return flying;
	}


	//public ArrayList<MovingEntity> getProjectiles() {
	//	return projectile;
	//}

	//not needed, as client updates its own stuff
	/*public void actFlying(){
		for(ServerToClientConnection in:flying){
			if(in!=null&&in.getEntity().isVisible())
				in.getEntity().act();
		}
	}*/

	/**
	 * makes all projectiles act
	 */
	private void actProjectiles(){
		synchronized(projectile){
			for(int i=0;i<this.projectile.size();i++){
				if(projectile.get(i)!=null&&projectile.get(i).isVisible())
					projectile.get(i).act();
			}
		}
	}
	//	private int i = 0;
	public void act(){
		this.collisionDetect();
		//if(i++ % 60 == 0 && ns.hasClientUpdates())
		//	flying = ns.getConnections();
		for(ServerToClientConnection c: flying){
			if(c.getEntity().isFiringBullets()){
				addProjectile(c.getEntity().spawnBullet());
			}
			if(getClosestEnemy(c.getEntity())!=null){
				if(c.getEntity().getMissileCount()>0&&c.getEntity().isMissileLaunching()){
					Vector2D temp=new Vector2D(c.getEntity().getX(),c.getEntity().getY(),
							c.getEntity().getRotation(),c.getEntity().getWidth()/2.0,true);
					addProjectile(new HomingEntity(temp.getX2(),temp.getY2(),10,this.getClosestEnemy(c.getEntity()),
							c.getEntity().getRotation()));
				}
				c.getEntity().setMissileLaunch(false);
			}
		}
		this.actProjectiles();
		//isDirty.set(true);
	}

	/**
	 * Basically, dont use this unless you know what you are doing
	 * @param list
	 */
	public void setFlying(List<ServerToClientConnection> list){
		flying = list;
	}

	/**
	 * Mainly for the missiles (homingEntities)
	 * @param checkWith
	 * @return
	 */
	public FlyingEntity getClosestEnemy(FlyingEntity checkWith){
		double closestDistance=Double.MAX_VALUE;
		FlyingEntity temp=null;
		for(ServerToClientConnection find:flying){
			if(find.getEntity()!=checkWith){
				if(find.getEntity().distanceTo(checkWith)<closestDistance){
					closestDistance=find.getEntity().distanceTo(checkWith);
					temp=find.getEntity();
				}
			}
		}
		return temp;

	}

	/**
	 * crude collision detection. Health deduction will also occur here
	 */
	private void collisionDetect(){
		for(int i=0;i<flying.size();i++){
			if(flying.get(i).getEntity().getY()>Settings.getHeight()-70)
				flying.get(i).getEntity().setHealth((short) 0);

			for(int j=0;j<projectile.size();j++){
				if(flying.get(i).getEntity().getHealth()>0
						&&
						flying.get(i).getEntity().isPointInside(projectile.get(j).getX(), projectile.get(j).getY())){
					if(projectile.get(j) instanceof BulletEntity){
						flying.get(i).getEntity().takeDamage(BulletEntity.damage);
					}
					else if(projectile.get(j) instanceof HomingEntity){
						flying.get(i).getEntity().takeDamage(HomingEntity.damage);
					}
					projectile.remove(j);
				}
			}
		}
	}

}
