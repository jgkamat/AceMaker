package org.ace.network;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;

import org.ace.entity.FlyingEntity;

import org.ace.util.Util;

/**
 * A class that represents the GUI portion of the client
 * The "network" client is part of the updater method in GameGui
 * @author AceMaker Team
 * @version 0.7
 *
 */
public class Client extends Thread{
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private Socket s;
	private boolean exiting;
	//private FlyingEntity plane;
	private ArrayBlockingQueue<Command> quene;
	
	/**
	 * Constructs a client
	 * @param host server IP
	 * @param port port # (4444)
	 * @throws UnknownHostException Thrown when host cannot be found
	 * @throws IOException Some other network problem
	 */
	public Client(String host, int port) throws IOException{
		if("".equals(host))
			host = "localhost";
		super.setName("Client to:" + host + " on " + port);
		s = new Socket(host, port);
		oos = new ObjectOutputStream(s.getOutputStream());
		quene = new ArrayBlockingQueue<Command>(100, true);
		exiting = false;
		this.start();
	}
	
	/**
	 * Sends a command to the server
	 * @param c the command to send
	 */
	public void send(Command c){
		if(exiting)
			return;
		try {
			oos.writeObject(c);
		}catch(SocketException se){
			Util.logW(se.toString());
			stopThread();
		} catch (IOException e) { 
			Util.logW("Error writing object", e);
		}
	}
	
	/* *
	 * Sets the client's plane
	 * @param fly the plane to set to
	
	public void setPlane(FlyingEntity fly){
		plane=fly;
	} */
	
	/**
	 * The method to make the server run. This method takes care of most network procedures
	 */
	public void run(){
		try{
			ois = new ObjectInputStream(s.getInputStream());
		}catch(IOException ioe){
			Util.logW("Problem Opening Stream", ioe);
			return;
		}
		//TODO: validate same CommandParser.
		while(!s.isClosed() && !exiting){
			Object x = null;
			try{
				Command o = (Command)(x = ois.readObject());
				if(o instanceof CommandDisconnect){
					exiting = true;
					Util.logI("Server shutting down");
					break;
				}
				if(!quene.offer(o)){
					Util.logW("Client backed up. Waiting to catch up.");
					try{
						quene.put(o);
					}catch(InterruptedException ie){
						Util.logW("interrupted while pushing; shutting down");
						return;
					}
				}
			}catch(SocketException e){
				if("Connection reset".equals(e.getMessage())){
					exiting = true;
					stopThread();
					Util.logW("Connection terminated", e);
				}else{
					Util.logW("error reading stream", e);
				}
			}catch(EOFException e){
				exiting = true;
				stopThread();
			}catch(IOException ioe){
				Util.logW("error reading stream", ioe);
			}catch(ClassCastException cce){
				Util.logW("error casting object: "+ x.toString(), cce);
			}catch (ClassNotFoundException e) {
				Util.logW("UNKNOWN PROBLEM", e);
				return;
			}
		}
	}
	/**
	 * Shuts down this client-connection.
	 */
	public void stopThread(){
		try {
			if(!exiting){
				if(!s.isClosed())
					try{
						send(new CommandDisconnect());
					}catch(Exception swallowed){}
				exiting = true;
			}
			s.close();
		} catch (IOException ioe) {
			Util.logW("Problem closing socket", ioe);
		}
	}
	
	/**
	 * Gets an update
	 * @return the update
	 */
	public Command getUpdate() {
		return quene.poll();
	}
	
	/* *
	 * Makes the client's entity act. This does not display him on the screen in any way
	 
	public void act(){
		plane.act();
	}*/
}
