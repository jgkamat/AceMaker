package org.ace.network;

/**
 * This command is a request to disconnect.
 * 
 * @author Andrew
 * @version 2013.5.12
 */
public final class CommandDisconnect extends Command{
	public static String name = "Disconnect";
	/** 
	 * Constructs a command disconnect object.
	 * 
	 */
	public CommandDisconnect(){
	}
	
	@Override
	public String name() {
		return name;
	}
	
}
