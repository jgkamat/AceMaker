package org.ace.network;

/**
 * Command for the server to send to the client to give it it's max health.
 * @author Jay Kamat
 *
 */
public class CommandSendMaxHealth extends Command{
	public static String name = "SendMaxHealth";

	public short maxHealth;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7887641918417062885L;
	
	public CommandSendMaxHealth(short currentDam){
		this.maxHealth=currentDam;
	}

	@Override
	public String name() {
		return name;
	}
}
