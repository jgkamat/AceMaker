package org.ace.network;

import org.ace.entity.FlyingEntity;

/**
 * A class that represents a command for a client to give the server its plane
 * Usefull for: initial startup, plane changes. Anything where the plane itself changes, other 
 * than rotation and location
 * @author jay
 *@version 2013.5.19
 */
public class CommandGiveEntity extends Command {
	public static String name = "GiveEntity";
	/**
	 * 
	 */
	private static final long serialVersionUID = -6271873959584039669L;
	public FlyingEntity plane;
	
	public CommandGiveEntity(FlyingEntity plane){
		this.plane=plane;
	}

	@Override
	public String name() {
		return name;
	}
	
}
