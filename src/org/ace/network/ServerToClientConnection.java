package org.ace.network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.Collection;

import org.ace.entity.FlyingEntity;

import org.ace.util.Util;

/** 
 * This class represents the server's connection to the client. Either side can
 * terminate the arrangement. Any updates that the client makes are pushed to a
 * waitlist on the server, accessible by the received() function.
 * 
 * @author Andrew
 * 
 */
public class ServerToClientConnection extends Thread{
	//private static int connectionNum = 1;
	private NetworkServer server;
	private Socket socket;
	private volatile boolean exiting;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private FlyingEntity clientEntity;
	private ArrayBlockingQueue<Command> quene;
	/**
	 * Constructs object representing the client's connection to the server.
	 * @param server The server.
	 * @param s The socket.
	 */
	public ServerToClientConnection(NetworkServer server, Socket s) {
		super("ServerToClient@" + s.getLocalPort());
		this.server = server;
		this.socket = s;
		quene = new ArrayBlockingQueue<Command>(60, true);
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
		} catch (IOException ioe) {
			Util.logW("Problem opening outputstream", ioe);
			try {
				s.close();
			} catch (IOException e2) {
				Util
						.logW("Problem closing socket after failing to open outputstream",
								e2);
			}
		}
		super.setDaemon(true);
		super.start();
		send(UpdateThread.getGame().getHandshake(this));
	}
	
	/**
	 * @see Thread#run()
	 * @see Thread#start()
	 */
	public void run(){
		//server.blockedClientPush(new CommandConnect(clientID));
		try{
			ois = new ObjectInputStream(socket.getInputStream());
		}catch(IOException ioe){
			Util.logW("Problem Opening Stream", ioe);
			return;
		}
		//TODO: validate same CommandParser.
		while(!socket.isClosed() && !exiting){
			Object o = null;
			try{
				Command x;
				try{
					x = (Command)(o = ois.readObject());
				}catch(IOException e){
					if(socket.isClosed() || exiting)
						return;
					else
						throw e;
				}catch(ClassCastException cce){
					Util.logW("CCE " + o.getClass(), cce);
					continue;
				}
				if(x instanceof CommandDisconnect){
					exiting = true;
					stopThread();
					break;
				}
				if(!quene.offer(x)){
				//if(!server.pushClientMessage(x)){
					Util.logW("Server backed up. Waiting on Server");
					try{
						quene.put(x);
						//server.blockedClientPush(x);
					}catch(InterruptedException ie){
						Util.logW("interrupted while pushing; shutting down");
						stopThread();
						return;
					}
				}
			}catch(SocketException e){
				if("Connection reset".equals(e.getMessage())){
					server.remove(this);
					try {
						socket.close();
					} catch (IOException e1) {
						Util.logW("Unable to close socket", e1);
					}
					Util.logI("Connection terminated " + socket.toString());
					exiting = true;
				}else if ("socket closed".equals(e.getMessage())){
					exiting = true;
				}else{
					Util.logW("error reading stream", e);
				}
			}catch(IOException ioe){
				Util.logW("error reading stream", ioe);
			}catch(ClassCastException cce){
				Util.logW("error casting object: "+ o.toString(), cce);
			}catch (ClassNotFoundException e) {
				Util.logW("UNKNOWN PROBLEM", e);
				return;
			}
		}
	}
	/**
	 * Sends the command to the client.
	 * @throws IOException 
	 */
	public void send(Command o){
		try {
			oos.writeObject(o);
			oos.flush();
			oos.reset();
		} catch (IOException ioe) {
			Util.logW("problem writing "+ o, ioe);
		}
	}
	/**
	 * Shuts down this client-connection.
	 */
	public void stopThread(){
		try {
			server.remove(this);
			if(!exiting){
				exiting = true;
				try{
					send(new CommandDisconnect());
				}catch(Exception swallowed){}
			}
			socket.close();
			
		} catch (IOException ioe) {
			Util.logW("Problem closing socket", ioe);
		}
	}
	/**
	 * Checks if this connection is still alive.
	 * @return if the connection is alive.
	 */
	public boolean isValidConnection(){
		return !exiting;
	}
	/**
	 * Drains the entire queue to the specified collection.
	 * @see java.util.concurrent.ArrayBlockingQueue#drainTo(Collection)
	 * @param c
	 */
	public void drainQueue(Collection<Command> c){
		quene.drainTo(c);
	}

	/**
	 * Retrieves and removes the next command in the queue. If the queue is
	 * empty, this method returns null.
	 * @see java.util.concurrent.ArrayBlockingQueue#poll()
	 * @return The next command in the queue.
	 */
	public Command received(){
		return quene.poll();
	}
	
	/**
	 * Each connection has an entity that "is" the client. Set it here
	 * @param in
	 */
	public void setEntity(FlyingEntity in){
		clientEntity=in;
	}
	
	/**
	 * Each connection with a client has an Entity. Get it here
	 * @return The client's entity.
	 */
	public FlyingEntity getEntity(){
		return this.clientEntity;
	}
	public String toString(){

		return "EXIT = " + exiting + ", Socket = " + this.socket.toString() +", Host = " + this.server.toString();

	}

}
