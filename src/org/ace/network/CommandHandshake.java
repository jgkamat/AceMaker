package org.ace.network;

import java.util.ArrayList;

import org.ace.entity.Entity;

/**
 * A class sent by th server to give the client neccisary information
 * @author AceMaker Team
 * @version 2013.5.19
 */
public class CommandHandshake extends Command {
	public static String name = "Handshake";
	private static final long serialVersionUID = 1L;
	public ArrayList<Entity> enviroment;
	public String mapName;
	public int viewingRadius;
	
	public CommandHandshake(ArrayList<Entity> enviro, String mapName, int viewingRadius){
		enviroment=enviro;
		this.mapName = mapName;
		this.viewingRadius = viewingRadius;
	}

	@Override
	public String name() {
		return name;
	}
	
}
