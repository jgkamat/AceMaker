package org.ace.network;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.concurrent.atomic.AtomicBoolean;

import org.ace.util.Util;
import org.ace.util.Settings;

/**
 * Its a network! Server!
 * @author Andrew
 *
 */
public class NetworkServer extends Thread{
	//PLEASE SEE THE FOLLOWING FOR WHAT THIS IS BASED ON.
	//http://www.cn-java.com/download/data/book/socket_chat.pdf
	//private ArrayBlockingQueue<Command> quene;
	private ServerSocket ss;
	private List<ServerToClientConnection> connections;
	private boolean shutdown;
//	public static NetworkServer s;
	private int maxConnections;
	private AtomicBoolean hasUpdates;

	/**
	 * Opens the server with the following defaults:port = 4444, hasGraphics =
	 * false, saveLocation {@link Util#getSaveLocation()}, numPeople
	 * {@link Util#getMaxPeople()}
	 * 
	 * @throws IOException
	 *             If there is an IOException thrown while opening the server on
	 *             the specified port.
	 */
	public NetworkServer() throws IOException{
		this(4444);
	}
	
	/**
	 * 
	 * @see Util#getSaveLocation()
	 * @see Util#getMaxPeople()
	 * @see #NetworkServer(int, boolean, String, int)
	 * @param port
	 *            The port to open the server on.
	 * @throws IOException
	 *             If there is an IOException thrown while opening the server on
	 *             the specified port.
	 */
	public NetworkServer(int port) throws IOException{
		this(port,  Settings.getSaveLocation(),Settings.getMaxPeople());
	}
	public NetworkServer(int port, String saveLoc) throws IOException{
		this(port,  saveLoc, Settings.getMaxPeople());
	}
	public NetworkServer(int port, int numPeople) throws IOException{
		this(port,  Settings.getSaveLocation(), numPeople);
	}
	
	/**
	 * Creates a NetworkServer with the given parameters.
	 * 
	 * @param port
	 *            The port to open the server on.
	 * @param saveLocation
	 *            The location to save, currently not implemented.
	 * @param numPeople
	 *            The max number of people that can connect to the server.
	 * @throws IOException
	 *             If there is an IOException thrown while opening the server on
	 *             the specified port.
	 */
	public NetworkServer(int port, String saveLocation, int numPeople)throws IOException{
		super("NetworkServer hosting on" + port);
//		if(s != null)
//			throw new RuntimeException();
//		s = this;
		//quene = new ArrayBlockingQueue<Command>(60*numPeople, true);
		connections = Collections.synchronizedList(new ArrayList<ServerToClientConnection>(numPeople));
		maxConnections = numPeople;
		// Create the ServerSocket
		hasUpdates = new AtomicBoolean(false);
		ss = new ServerSocket(port);
		this.start();
		//new UpdateThread().start();
	}
	
	/**
	 * Shuts down the server
	 * 
	 * @throws IOException If there is an IOException closing the server.
	 */
	public void shutdown() throws IOException{
		if(shutdown)
			return;
		shutdown = true;
		ss.close();
		synchronized (connections){
			for(ServerToClientConnection s: connections){
				s.stopThread();
			}
			connections.clear();
		}
	}
	/**
	 * @see NetworkServer#listen()
	 * @see Thread#run()
	 * @see Thread#start()
	 */
	@Override public void run(){
		listen();
		Util.logW("NetworkServerThread: DEATH");
	}

	/**
	 * Sends a command to all the clients. If a command only needs to be sent to
	 * one client, that could be sent through the {@link org.ace.network.ServerToClientConnection#send(Command)}
	 * 
	 * @see ServerToClientConnection#send(Command)
	 * @see NetworkServer#getConnections()
	 * 
	 * @param c
	 *            The command to send to the clients.
	 * @throws IOException
	 *             If there is a problem writing the command.
	 */
	public void sendToAll(Command c) throws IOException{
		synchronized (connections) {
			for(ServerToClientConnection sc: connections){
				sc.send(c);
			}
		}
	}
	/*
	 * Sends all of the commands inside of a Hashmap to related integer.
	 * @param hm
	 * /
	public void sendCommands(HashMap<Serializable, Integer> hm){
		Set<Map.Entry<Serializable, Integer>> x = hm.entrySet();
		for(Map.Entry<Serializable, Integer> i: x){
			synchronized (connections){
			for(ServerToClientConnection j: connections){
				if(j.getID() == i.getValue()){
					j.send (i.getKey());
					break;
				}
			}
			}
		}
	}*/

	/**
	 * Removes a specified ServerToClientConnection from this server. Does not
	 * attempt to close it. This method should only be called if a
	 * org.ace.communication.CommandDisconnect has already been sent.
	 * 
	 * @see org.ace.network.CommandDisconnect
	 * @see ServerToClientConnection#stopThread()
	 * 
	 * @param s
	 *            The socket to remove.
	 */
	public void remove(ServerToClientConnection s){
		if(shutdown)
			return;
		hasUpdates.set(true);
		connections.remove(s);
	}
	
	/**
	 * Gets a list of all the connections to this server.
	 * @return a java.util.ArrayList<ServerToClientConnection> with all the connections.
	 */
	public List<ServerToClientConnection> getConnections(){
		ArrayList<ServerToClientConnection> result = new ArrayList();
		synchronized(connections){
			result.addAll(connections);
		}
		return result;
	}
	
	/* *
	 * Drains the entire queue to the specified collection.
	 * @see java.util.concurrent.ArrayBlockingQueue#drainTo(Collection)
	 * @param c
	 * /
	public void drainQueue(Collection<Object> c){
		quene.drainTo(c);
	}* /

	/**
	 * Retrieves and removes the next command in the queue. If the queue is
	 * empty, this method returns null.
	 * @see java.util.concurrent.ArrayBlockingQueue#poll()
	 * @return The next command in the queue.
	 * /
	public Command received(){
		return quene.poll();
	}

	/**
	 * This pushes the command to the server waitlist, returning true if it was
	 * successful, false if it was not, and logging a warning if it's nearing
	 * capacity.
	 * 
	 * @param c
	 *            The command to push to the server.
	 * @return true if successful in pushing command.
	 * /
	protected boolean pushClientMessage(Command o){
		if (quene.remainingCapacity() < 10)
			LoggingUtil.logW("Quene@" + quene.remainingCapacity() + ", Thread@"
					+ Thread.currentThread().getName(), null);
		return quene.offer(o);
	}

	/**
	 * This forces the client command to the waitlist, blocking if the hold is
	 * full.
	 * 
	 * @param x
	 *            The command to push to the waitlist.
	 * @throws InterruptedException
	 *             If the thread is interrupted while waiting.
	 * /
	protected void blockedClientPush(Command x) throws InterruptedException{
		quene.put(x);
	}*/

	/**
	 * This method was borrowed from online, then modified to better suit this
	 * class.
	 * 
	 * @param port
	 *            The port to open the server on.
	 * @throws IOException
	 *             If there is a problem opening the server on the specified
	 *             port. Such as if the port is already bound.
	 */
	private void listen(){
		// Keep accepting connections as long as the server's open.
		mainLoop: while (!ss.isClosed()) {
			// Grab the next incoming connection
			Socket s;
			try{
				Util.logI("Listening on " + ss);
				s = ss.accept();
			}catch(SocketException se){
				if(ss.isClosed())
					break;
				else
					throw new RuntimeException(se);
			}catch(IOException ioe){
				Util.logW("IO problem accepting connection", ioe);
				continue;
			}
			// Tell the world we've got it
			Util.logI("Connection from " + s);
			// Add this connection to the list of clients.
			connections.add(new ServerToClientConnection(this, s));
			hasUpdates.set(true);
			
			if(maxConnections == 1) // Is singleplayer
				return;
			while(connections.size() >= maxConnections){
				try{
					Thread.sleep(1000);
				}catch(InterruptedException ie){
					break mainLoop;
				}
			}
		}
	}
	/**
	 * Checks if there are new clients for the server.
	 * @return true if another client has connected, false otherwise.
	 */
	public boolean hasClientUpdates(){
		return hasUpdates.getAndSet(false);
	}
	public boolean hasClientUpdates(List<ServerToClientConnection> checkAgainst){
		if(checkAgainst.size() != connections.size())
			return true;
		synchronized(connections){
			for(int i = 0; i < checkAgainst.size(); i ++){
				if(!checkAgainst.get(i).equals(connections.get(i)))
					return true;
			}
		}
		return false;
	}
}
