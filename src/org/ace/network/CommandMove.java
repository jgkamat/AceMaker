package org.ace.network;

import org.ace.phys.Vector2D;

/**
 * Sent by the client to tell the server where its "plane" has moved
 * @author AceMaker Team
 * @version 2013.5.19
 *
 */
public class CommandMove extends Command {
	public static String name = "Move";
	public double x,y,rotation;
	public boolean firingBullet;
	public short missileCount;
	public Vector2D vel;
	
	public CommandMove(double x, double y, double rotation, boolean isFiring, short missileCount, Vector2D vel){
		this.x=x;
		this.y=y;
		this.rotation=rotation;
		this.firingBullet=isFiring;
		this.vel=vel;
		this.missileCount=missileCount;
	}

	@Override
	public String name() {
		return name;
	}
}
