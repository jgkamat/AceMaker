package org.ace.network;

import java.util.ArrayList;
import java.awt.Image;
import java.io.IOException;

import org.ace.entity.EntityManager;
import org.ace.entity.FlyingEntity;
import org.ace.planes.F22;
import org.ace.planes.Biplane;
import org.ace.util.Settings;
import org.ace.util.Util;

public class UpdateThread extends Thread{

	private EntityManager manage;
	private volatile static UpdateThread self;
	private final NetworkServer ns;
	private long sleepTime = 1000 / 60;
	public UpdateThread() throws IOException{
		this(4444, 1);
	}
	public UpdateThread(int port, int numMax) throws IOException{
		super("UpdateThread");
		if(self != null)
			throw new RuntimeException();
		ns = new NetworkServer(port, numMax);
		self = this;

		self.start();
	}

	/**
	 * Starts the server thread that does game updates.
	 * @see Thread#run()
	 * @see Thread#start()
	 */
	public void run(){

		//~Pretend this is your main method.~
		//Do a setup (get the world ready, make a note of any parameters you need, etc)
		manage=new EntityManager(ns);
		//Loop if not shutdown

		ArrayList<Command> stuff=new ArrayList<Command>();
		try {
			long lastUpdate = 0;
			while (!this.isInterrupted()) {
				int dirty = 0;
				//time = System.currentTimeMillis();
				if (ns.hasClientUpdates()) {
					manage.setFlying(ns.getConnections());
				}
				for (ServerToClientConnection scc : manage.getClients()) {
					scc.drainQueue(stuff);
					//System.out.println(scc + " has " + stuff.size());
					for (Command c : stuff) {
						dirty++;
						// Process the commands
						if (c instanceof CommandGiveEntity) {
							FlyingEntity e = ((CommandGiveEntity) c).plane;
							scc.setEntity(e);
							if(e instanceof F22){
								scc.send(new CommandSendMaxHealth((short)1000));
							}else if(e instanceof Biplane){
								scc.send(new CommandSendMaxHealth((short)500));
							}else{
								throw new RuntimeException();
							}
						} 
						else if (c instanceof CommandMove) {
							CommandMove coms = (CommandMove) c;
							FlyingEntity e = scc.getEntity();
							e.setX(coms.x);
							e.setY(coms.y);
							e.setRotation(coms.rotation);
							e.setBulletFire(coms.firingBullet);
							e.setVel(coms.vel);
							if(coms.missileCount<e.getMissileCount()){
								e.setMissileLaunch(true);
							}
							e.setMissileCount(coms.missileCount);
						} 
						else {
							throw new RuntimeException("For client"
									+ scc.toString() + "For this:"
									+ c.getClass() + ", " + c.toString());
						}
					}
					stuff.clear();
				}
				if((System.currentTimeMillis() - lastUpdate) >= sleepTime){
					manage.act();
					lastUpdate = System.currentTimeMillis();
					dirty++;
				}

				//Process all updates, deaths, everything.
				//Recieved Connect: ID > ConstructEntity;
				//reply with EntitySelf

				//--Recieved Move: ID, x, y > Reflect move.
				//--Recieved Firing: ID
				//Recieved DisConn: 
				//Remove client

				if (dirty != 0) {
					for (ServerToClientConnection name : manage.getClients()){
						name.send(new CommandUpdate(manage.getAround(name
								.getEntity())));
						name.send(new CommandSendDamage(name.getEntity().getHealth()));
					}
					yield();
				} else
					Thread.sleep(1);
				//Queue the sending of everything in Server.send();
				//long st = sleepTime - (System.currentTimeMillis() - time);
				//if(st > 0)
				//	sleep(st);
				//else

			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//Set a code to say exit in progress.
		//Write out everything
		//Tell server to notify clients of exit (assuming a networking error isn't cause)
		self = null;
		try{
			//			ns.sendToAll(new CommandDisconnect());
			ns.shutdown();
		}catch(IOException e){
			e.printStackTrace();
		}
		Util.logI("UpdateThread dying, printing Threads");
		Util.printThreads();
	}
	public NetworkServer getNetworkServer(){
		return ns;
	}

	/**
	 * Makes a commandHandshake to send to a unsuspecting client. TAKE THAT!
	 * @param abc 
	 * @return it contains static info about the world...hopefully
	 */
	public CommandHandshake getHandshake(ServerToClientConnection abc){
		//this is just a temp plane, while the server is waiting for a commandGiveEntity
		FlyingEntity clientStuff=new F22(10,10,10,10);
		abc.setEntity(clientStuff);
		Image background = Util.getImage("background.jpg");
		Settings.setWidth(background.getWidth(null));
		Settings.setHeight(background.getHeight(null));
		return new CommandHandshake(manage.getStationary(), "background.jpg", (int)EntityManager.lookingThreshold);
	}

	/**
	 * Welll... you see... OOP is hard. And theres only one of these soooooo...
	 * @return the ONLY EVER {@link UpdateThread} in existence ever in this program.
	 */
	public static UpdateThread getGame(){
		return self;
	}
}
