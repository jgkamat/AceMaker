package org.ace.network;

import java.util.ArrayList;

import org.ace.entity.Entity;

/**
 * Sent by the server to tell the client updates that have happened
 * Basically sends all of the stuff that the client is able to see
 * @author AceMaker Team
 * @version 2013.5.19
 *
 */
public class CommandUpdate extends Command{
	public static String name = "Update";
	
	
	//TODO this class may make networking slow
	
	public Entity[] updates;
	private static int update = Integer.MIN_VALUE;
	public int updateNum;
	
	public CommandUpdate(ArrayList<Entity> in){
		updates=new Entity[in.size()];
		for(int i=0;i<in.size();i++){
			updates[i]=in.get(i);
		}
		updateNum = update++;;
	}
	public boolean equals(Object other){
		return false;
	}
	/*
	public boolean deepCheck(CommandUpdate other){
		if(other.updates.length != this.updates.length)
			return false;
		for(int i = 0; i < updates.length; i++)
			if(updates[i].equals)
	}*/

	@Override
	public String name() {
		return name;
	}
}
