package org.ace.network;

/**
 * Command for the server to send to the client to update its current health.
 * @author Jay Kamat
 *
 */
public class CommandSendDamage extends Command{
	public static String name = "SendDamage";

	public short health;
	/**
	 * 
	 */
	private static final long serialVersionUID = -7887641918417062885L;
	
	public CommandSendDamage(short currentDam){
		this.health=currentDam;
	}

	@Override
	public String name() {
		return name;
	}
}
