package org.ace.network;

import java.io.Serializable;

/**
 * A command is the most basic version of anything that can be
 * transmitted over the network
 * @author AceMaker Team
 * @version 2013.5.19
 */
public abstract class Command implements Serializable{
	/**
	 * Meepers Jeepers! Its serializable!
	 */
	private static final long serialVersionUID = -2844882208023246562L;
	/**
	 * Returns the (reduced) name of the class.
	 */
	public abstract String name();
}
