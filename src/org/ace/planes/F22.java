package org.ace.planes;

import org.ace.entity.FlyingEntity;

/**
 * A class to represent a F-22 Fighter Jet.
 * 
 * GO LOCKHEED MARTIN! 
 * <please dont sue me>
 * 
 * All it does is set variables in the FlyingEntity class
 * @author Jay Kamat
 * @version 5.26.2013
 *
 */
public class F22 extends FlyingEntity {

	private static final long serialVersionUID = -1857142110386988026L;

	/**
	 * Does fancy things like set thrust and lift for you!
	 * @param x
	 * @param y
	 * @param w
	 * @param h
	 */
	public F22(double x, double y, double w, double h) {
		super(x, y, w, h, 10.0, 10.0,.1,1/75.0);
	}

}