package org.ace.planes;

import org.ace.entity.FlyingEntity;

/**
 * May or may not be implemented fully. Its supposed to represent a biplane. 
 * Its an alternative (be it a stupid one) to the F-22
 * @author Jay Kamat
 *
 */
public class Biplane extends FlyingEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -560904303713937276L;


	public Biplane(double x, double y, double w, double h) {
		super(x, y, w, h, 01, 0.3, .01, 1.0/20);
	}

}
