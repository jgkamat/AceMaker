package org.ace.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import java.io.IOException;

import java.util.List;

import javax.swing.border.TitledBorder;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import org.ace.network.UpdateThread;
import org.ace.network.ServerToClientConnection;

import org.ace.util.Settings;
import org.ace.util.Util;

/**
 * It is a window for the server.
 *
 */
public class ServerHead extends JFrame{
	private UpdateThread ut;
	private List<ServerToClientConnection> list;
	private DefaultListModel<ServerToClientConnection> model;
	private JList<ServerToClientConnection> datList;
	public ServerHead() throws IOException{
		ut = new UpdateThread(Settings.getDefaultPort(), 10);
		addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent e){
				while(ut.isAlive())
					ut.interrupt();
					Util.printThreads();
			}
		});
		Timer t = new Timer(1000, new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				SwingUtilities.invokeLater(new Runnable(){
					public void run(){
						updateList();
					}
				});
				
			}
		});
		t.start();
		setupGui();
	}
	
	
	private void setupGui(){
		model = new DefaultListModel<ServerToClientConnection>();
		updateList();
		datList = new JList<ServerToClientConnection>(model);
		JScrollPane sp = new JScrollPane(datList, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		sp.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),"Connections: ", TitledBorder.LEFT, TitledBorder.TOP));
		JButton b1 = new JButton("Kick User");
		b1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				List<ServerToClientConnection> kicks = datList.getSelectedValuesList();
				for(ServerToClientConnection scc: kicks){
					scc.stopThread();
				}
				updateList();
			}});
		Box box1 = Box.createHorizontalBox();
		box1.add(new JLabel("Hosting on: "));
		for(String s: Util.getConnections()){ // ALL THAT READABLE CODE!!
			box1.add(new JLabel(s));
			box1.add(new JLabel("   "));
		}
		box1.add(b1);
		Box box2 = Box.createVerticalBox();
		box2.add(sp);
		box2.add(box1);
		this.add(box2);
	}
	private void updateList(){
		if(list == null || ut.getNetworkServer().hasClientUpdates(list)){
			list = ut.getNetworkServer().getConnections();
			model.removeAllElements();
			//System.out.println(list.size());
			for(ServerToClientConnection scc: list)
				model.addElement(scc);
		}
	}
}
/*
class ListString extends JFrame {

    private static final long serialVersionUID = 1L;
    private DefaultListModel<String> model = new DefaultListModel();
    private int i = 01;

    public ListString() {
        model.addElement(("one" + i++));
        model.addElement(("two" + i++));
        model.addElement(("three" + i++));
        model.addElement(("four" + i++));
        JList list = new JList(model);
        add(new JScrollPane(list));
        JButton btn = new JButton("Remove All Rows :");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                model.removeAllElements();
            }
        });
        add(btn, BorderLayout.SOUTH);
        JButton btn1 = new JButton("Add New Rows:");
        btn1.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                model.addElement(("one" + i++));
                model.addElement(("two" + i++));
                model.addElement(("three" + i++));
                model.addElement(("four" + i++));
            }
        });
        add(btn1, BorderLayout.NORTH);
    }

    public static void main() {
        //UIManager.getLookAndFeelDefaults().put("List.selectionBackground", Color.red);
        ListString frame = new ListString();
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}*/
