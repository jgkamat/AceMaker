package org.ace.gui;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.ace.util.JayLayer;
import org.ace.util.Settings;
import org.ace.util.Util;

import org.ace.network.Client;
import org.ace.network.UpdateThread;

public class AceMaker extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final String LAYOUT_TITLE = "Title", LAYOUT_GAME = "Game";
	public static final String VERSION = "0.2B r1";

	private CardLayout title;
	private JPanel everything;
	private GameGui gameGraphics;

	private JayLayer player;

	private UpdateThread ut;

	private static AceMaker only;

	public AceMaker() {
		super("AceMaker Version: " + VERSION);
		if(only == null)
			only = this;
		else
			throw new RuntimeException("DON'T STOP DA MUSIC!");

		title = new CardLayout();
		everything = new JPanel(title);

		JPanel centering = new JPanel() {
			private static final long serialVersionUID = 1L;

			public void paintComponent(Graphics g) {
				super.paintComponent(g);
				Graphics2D g2 = (Graphics2D) g;
				g2.drawImage(img, 0, 0, getWidth(), getHeight(), null);
				g2.setColor(new Color(0,0,0,alpha));
				g2.fillRect(0,0, getWidth(), getHeight());
			}
		};
		centering.setLayout(new BoxLayout(centering, BoxLayout.Y_AXIS));
		centering.setDoubleBuffered(true);
		centering.setOpaque(false);
		setSlideshow(Util.getJPGImages("res/slide", centering));

		centering.add(new TitleImage(centering));
		centering.add(buildSelection());
		centering.setBackground(Color.DARK_GRAY);
		centering.setBounds(this.getBounds());

		everything.add(centering, LAYOUT_TITLE);
		gameGraphics = new GameGui();
		everything.add(gameGraphics, LAYOUT_GAME);
		setGraphicsVisible(LAYOUT_TITLE);

		this.setContentPane(everything);

		player = new JayLayer("res/sound/", "", true);
		player.addPlayList();
		player.addPlayList();
		player.addSong(0, "Menu1.mp3");
		player.addSong(0, "Silence.mp3");
		player.addSong(1, "Game1.mp3");
		player.addSong(1, "Game2.mp3");
		player.addSong(1, "Game3.mp3");
		player.addSong(1, "Game4.mp3");
		player.addSong(1, "Game5.mp3");
		player.addSong(1, "Game6.mp3");
		//player.setWait(1000);
		player.changePlayList(0);
	}
	private void setGraphicsVisible(String ti){
		switch(ti){
		case LAYOUT_TITLE:
		case LAYOUT_GAME:
			//case LAYOUT_ESCAPE:
			title.show(everything, ti);
			break;
		default:
			throw new RuntimeException();
		}
	}
	private void startupSingleplayer(){
		try{
			ut = new UpdateThread();
			Client c = new Client("localhost", 4444);
			gameGraphics.getUpdater().setClient(c);
			setGraphicsVisible(LAYOUT_GAME);
			gameGraphics.requestFocus();
		}catch(IOException e){
			e.printStackTrace();
		}
		player.changePlayList(1);
	}
	private void exitSingleplayer(){
		if(ut != null){
			while(ut.isAlive())
				ut.interrupt();
		}
		gameGraphics.getUpdater().setClient(null);
		setGraphicsVisible(LAYOUT_TITLE);
		player.changePlayList(0);
	}
	private void startupMultiplayer(String s) throws IOException{
		String[] arr = s.split(":");
		Client c;
		if(arr.length == 1){
			c = new Client(s, Settings.getDefaultPort());
		}else if(arr.length == 2){
			c = new Client(arr[0], Integer.parseInt(arr[1])); 
		}else
			throw new RuntimeException("Invalid connection String: " + s);
		gameGraphics.getUpdater().setClient(c);
		setGraphicsVisible(LAYOUT_GAME);
		player.changePlayList(1);
		gameGraphics.requestFocus();
	}

	private JComponent buildSelection() {
		JPanel optionsPane = new JPanel(null);
		final String[] names = { "Instructions", "Singleplayer", "Multiplayer",
				"Texture Packs", "Settings", "Quit Game" };
		CoolButton[] buttons = new CoolButton[names.length];
		for (int i = 0; i < buttons.length; i++)
			buttons[i] = new CoolButton(names[i]);
		Dimension pref = buttons[3].getPreferredSize();
		final CardLayout cl = new CardLayout();
		final JPanel respondingPane = new JPanel(cl);
		respondingPane.setOpaque(false);
		for (int i = 0; i < buttons.length; i++) {
			CoolButton b = buttons[i];
			final String sho = names[i];
			b.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					cl.show(respondingPane, sho);
				}
			});
			b.setBounds(0, i * pref.height, pref.width, pref.height);
			optionsPane.add(b);
		}
		optionsPane.setOpaque(false);
		optionsPane.setPreferredSize(new Dimension(pref.width, 300));

		{// Instructions
			JTextArea textArea = new JTextArea(10, 20);
			textArea.setBorder(BorderFactory.createEmptyBorder());
			textArea.setText(Util.getReadme());
			textArea.setLineWrap(true);
			textArea.setEditable(false);
			textArea.setOpaque(false);
			// textArea.setB
			JScrollPane jsp = new JScrollPane(textArea);
			jsp.setBorder(BorderFactory.createEmptyBorder());
			jsp.setOpaque(false);
			respondingPane.add(jsp, names[0]);
		}
		{// Singleplayer
			class StartPanel extends JPanel {
				private static final long serialVersionUID = 1L;
				BufferedImage i;
				private CoolButton cb;

				public StartPanel() {
					// super(null);
					super.setLayout(null);
					i = Util.getImage("play.jpg");
					double ratio = i.getWidth() / (double) i.getHeight();
					this.setPreferredSize(new Dimension((int) (ratio * 200),
							200));
					cb = new CoolButton("Begin game");
					cb.setLocation(getWidth() - cb.getWidth() - 30, getHeight()
							- cb.getHeight() - 10);
					cb.addActionListener(new ActionListener(){
						@Override
						public void actionPerformed(ActionEvent arg0) {
							startupSingleplayer();
						}});
					addComponentListener(new ComponentAdapter() {
						@Override
						public void componentResized(ComponentEvent e) {
							int wid = 200, hei = 25;
							cb.setBounds(getWidth() - wid - 30, getHeight()
									- hei - 10, wid, hei);
							repaint();
						}
					});
					add(cb);
				}

				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					g.drawImage(i, 0, 0, getWidth(), getHeight(), null);
				}

				public void setEnabled(boolean state) {
					cb.setEnabled(state);
				}
			}
			JPanel sp = new StartPanel();
			//sp.setEnabled(false);
			respondingPane.add(sp, names[1]);
		}
		{// Multiplayer
			// TODO: Get rid of copy-paste code.
			class MultiplayerQuestionPanel extends JPanel {
				private static final long serialVersionUID = 1L;
				private BufferedImage i;
				private CoolButton cb;
				private JTextField jtf;
				private JLabel jl;
				private volatile int buttonWidth = 200, textFieldWidth, height = 25, yIndent = 10;;
				private volatile IOException e;
				private volatile boolean needsUpdate;
				private volatile boolean isStartupRunning = false;
				private Image[] loading;

				public MultiplayerQuestionPanel() {
					super(null);
					i = Util.getImage("Multi.jpg");
					cb = new CoolButton("Connect");
					/*double ratio = i.getWidth() / (double) i.getHeight();
					this.setPreferredSize(new Dimension((int) (ratio * 200),
							200));

					cb.setLocation(getWidth() - cb.getWidth() - 30, getHeight()
							- cb.getHeight() - 10);*/
					cb.addActionListener(new ActionListener(){
						@Override
						public void actionPerformed(ActionEvent arg0) {
							if(isStartupRunning)
								return;
							else
								new Startup();
							needsUpdate = true;
							repaint();
						}});
					add(cb);
					jtf = new JTextField(20);
					jtf.setBorder(BorderFactory.createEmptyBorder());
					jtf.setOpaque(false);
					add(jtf);
					jl = new JLabel("Enter address to connect to: ");
					add(jl);
					textFieldWidth = jtf.getPreferredSize().width;
					addComponentListener(new ComponentAdapter() {
						@Override
						public void componentResized(ComponentEvent e) {
							needsUpdate = true;
							repaint();
						}
					});
					needsUpdate = true;
					loading = Util.getAllImages("res/loading", this);
				}


				private void updateLocations() {
					cb.setBounds(getWidth() - buttonWidth - 30, getHeight()
							- height - yIndent, buttonWidth, height);
					jtf.setBounds(getWidth() - buttonWidth - 40
							- textFieldWidth, getHeight() - height - yIndent,
							textFieldWidth, height);
					Dimension d = jl.getPreferredSize();
					jl.setBounds(
							getWidth() - buttonWidth - 40 - textFieldWidth,
							getHeight() - height - d.height - yIndent, d.width,
							d.height);
				}

				public void paintComponent(Graphics g) {
					super.paintComponent(g);
					if(needsUpdate)
						updateLocations();
					g.drawImage(i, 0, 0, getWidth(), getHeight(), null);
					g.setColor(new Color(200, 200, 200, 100));
					g.fillRect(getWidth() - buttonWidth - 40 - textFieldWidth,
							getHeight() - height - yIndent, textFieldWidth, height);
					Dimension d = jl.getPreferredSize();
					g.fillRect(getWidth() - buttonWidth - 40
							- textFieldWidth, getHeight()
							- height - d.height - yIndent,
							d.width, d.height);
					if(e != null){
						g.setColor(Color.RED);
						g.drawString("Error:"+ e.getClass().getSimpleName() +": " + e.getMessage(),getWidth() - buttonWidth - textFieldWidth, getHeight() - yIndent/2 );
					}
				}

				class Startup extends Thread {
					public Startup() {
						super("StartupMultiplayer");
						setDaemon(true);
						start();
					}

					@Override
					public void run() {
						cb.setChangingIcon(loading, 80);
						try {
							startupMultiplayer(jtf.getText());
						} catch (IOException ce) {
							e = ce;
							yIndent = 30;
							needsUpdate = true;
							repaint();
						}
						cb.setChangingIcon(null, 0);
						isStartupRunning = false;
					}
				}

				public void setEnabled(boolean state) {
					cb.setEnabled(state);
				}
			}
			JPanel sp = new MultiplayerQuestionPanel();
			//sp.setEnabled(false);
			respondingPane.add(sp, names[2]);
		}
		{//TexturePack, Settings
			buttons[3].setEnabled(false);
			buttons[4].setEnabled(false);
			//respondingPane.add(new JLabel(), names[3]);
			//respondingPane.add(new JLabel(), names[4]);
		}
		{//Quit
			class StartPanel extends JPanel {
				private static final long serialVersionUID = 1L;
				//BufferedImage i;
				private CoolButton cb;

				public StartPanel() {
					setOpaque(false);
					// super(null);
					super.setLayout(null);
					//i = ImageUtil.getImage("play.jpg");
					//double ratio = i.getWidth() / (double) i.getHeight();
					//this.setPreferredSize(new Dimension((int) (ratio * 200),
					//		200));
					cb = new CoolButton("Goodbye!");
					cb.setLocation(getWidth() - cb.getWidth() - 30, getHeight()
							- cb.getHeight() - 10);
					cb.addActionListener(new ActionListener(){
						public void actionPerformed(ActionEvent e) {
							System.exit(0);
						}
					});
					addComponentListener(new ComponentAdapter() {
						@Override
						public void componentResized(ComponentEvent e) {
							int wid = 200, hei = 25;
							cb.setBounds(getWidth() - wid - 30, getHeight()
									- hei - 10, wid, hei);
							repaint();
						}
					});
					add(cb);
				}

				//public void paintComponent(Graphics g) {
				//	super.paintComponent(g);
				//g.drawImage(i, 0, 0, getWidth(), getHeight(), null);
				//}

				public void setEnabled(boolean state) {
					cb.setEnabled(state);
				}
			}
			JPanel sp = new StartPanel();
			respondingPane.add(sp, names[5]);
		}

		Box b = Box.createHorizontalBox();
		b.add(optionsPane);
		b.add(Box.createHorizontalStrut(15));
		b.add(respondingPane);
		return b;
	}

	volatile int alpha;
	volatile Image img;

	public void setSlideshow(final Image[] in) {
		Thread t = new Thread("Slideshow"){
			public void run() {
				try {
					while (true) {
						for (Image i : in) {
							img = i;
							for (double a = 0; a <= 90; a += 2) {
								alpha = 255 - (int) (Math.sin(Math.toRadians(a)) * 255);
								repaint();
								Thread.sleep(50);
							}
							Thread.sleep(2500);
							for (double a = 90; a <= 180; a += 2) {
								alpha = 255 - (int) (Math.sin(Math.toRadians(a)) * 255);
								repaint();
								Thread.sleep(50);
							}
						}
					}
				} catch (InterruptedException e) {

				}
			}
		};
		t.setDaemon(true);
		t.start();
	}
	public static void returnToMain() {
		only.exitSingleplayer();
	}

}
