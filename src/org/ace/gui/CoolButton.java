package org.ace.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicButtonUI;
import static org.ace.util.Util.*;

/**
 * Its the best button in town
 * @author Andrew
 *
 */
public class CoolButton extends JButton {
	private Thread changingIcon;
	public CoolButton(){
		this(null);
	}
	public CoolButton(String s){
		super(s);
		this.setUI(new SpecialButtonUI());
	}
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
	}
	
	/**
	 * Sets things required for the button to have an image, and move.
	 * @param in Images
	 * @param delay delaytime
	 */
	public void setChangingIcon(final Image[] in,final int delay){
		if(changingIcon != null && changingIcon.isAlive())
			while(changingIcon.isAlive())
				changingIcon.interrupt();
		if(in == null){
			SwingUtilities.invokeLater(new Runnable(){
				@Override
				public void run() {
					setIcon(null);
			}});
			return;
		}
		changingIcon = new Thread(new Runnable(){
			@Override
			public void run() {
				try{
					Image[] graphics = new Image[in.length];
					for(int i = 0; i < in.length; i++)
						graphics[i] = in[i].getScaledInstance(20, 20, Image.SCALE_SMOOTH);
					final ImageIcon[] ii = new ImageIcon[in.length];
					for(int i = 0; i < ii.length; i++)
						ii[i] = new ImageIcon(graphics[i]);
					
					while(!changingIcon.isInterrupted()){
						for(final ImageIcon i: ii){
							SwingUtilities.invokeLater(new Runnable(){
								@Override
								public void run() {
									CoolButton.this.setIcon(i);
								}});
							Thread.sleep(delay);
						}
					}
				}catch(InterruptedException ie){
				}
			}
		}, "CoolButton changingIcon");
		changingIcon.setDaemon(true);
		changingIcon.start();
	}
}

class SpecialButtonUI extends BasicButtonUI {
	protected final int BASE_INDENT = 10;
	protected int slant = 15, indentSize = 10, actualIndentSpace = 0, animationSpeedFactor = 25;
	protected final Color fc = new Color(100, 150, 255, 150);
	protected final Color ac = Color.BLACK;//new Color(130, 130, 130);
	protected final Color bc = Color.DARK_GRAY, cc = Color.LIGHT_GRAY;
	protected final Color rc = new Color(235, 170, 0);
	protected final Color sc = new Color(255, 0,0, 150);
	protected Shape shape;
	protected Shape base;
	int height, width;
	
	@Override
	protected void installDefaults(AbstractButton b) {
		super.installDefaults(b);
		b.setContentAreaFilled(false);
		b.setOpaque(false);
		b.setBorderPainted(false);
		b.setBackground(new Color(250, 250, 250));
		b.setHorizontalTextPosition(SwingConstants.LEFT);
		initShape(b);
		Font f = b.getFont();
		//System.out.println(f.getFamily());
		Font f2 = new Font(f.getName(), Font.ITALIC | Font.BOLD, f.getSize());
		b.setFont(f2);
		b.setForeground(Color.WHITE);
	}

	@Override
	protected BasicButtonListener createButtonListener(AbstractButton b) {
		return new SpecialListener(b, shape); 
	}
	
	@Override
	public void paint(Graphics g, JComponent c) {
		Graphics2D g2 = (Graphics2D) g;
		AbstractButton b = (AbstractButton) c;
		ButtonModel model = b.getModel();
		initShape(b);
		// ContentArea
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		if(b.isEnabled())
			if (model.isArmed()) {
				g2.setColor(ac);
				Polygon l = new Polygon();
				l.addPoint(slant, 0);
				l.addPoint(width - BASE_INDENT + actualIndentSpace, 0);
				l.addPoint(width - BASE_INDENT + actualIndentSpace - slant, height);
				l.addPoint(0, height);
				g2.fill(l);
				paintFocusAndRollover(g2, b, darker(rc, .81), rc);
			} else if (b.isRolloverEnabled() && model.isRollover()) {
				paintFocusAndRollover(g2, b, darker(rc, .9), brighter(rc));
			} else if (b.hasFocus()) {
				paintFocusAndRollover(g2, b, fc);
			} else {
				paintFocusAndRollover(g2, b, sc);
				//g2.setColor(b.getBackground());
				//g2.fill(shape);
				//System.out.println(c);
			}
		else{
			g2.setColor(bc);
			Polygon l = new Polygon();
			l.addPoint(slant, 0);
			l.addPoint(width - BASE_INDENT, 0);
			l.addPoint(width - BASE_INDENT - slant, height-1);
			l.addPoint(0, height-1);
			g2.fill(l);
			
			g2.setColor(cc);
			g2.draw(l);
			//g2.drawRect(size, size, width-size*2-1, height-size*2-1);
			//g2.setColor(bc);
			//g2.fillRect(size*2, size*2, width - size*4, height-size*4);
			
		}
		// Border
		//g2.setPaint(c.getForeground());
		//g2.draw(shape);

		g2.setColor(b.getBackground());
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_OFF);
		super.paint(g2, b);
	}
	
	@Override
	protected void paintText(Graphics g, AbstractButton b, Rectangle textRect, String text) {
		if(b.isEnabled())
			textRect.x += actualIndentSpace - 10;
		paintText(g, (JComponent)b, textRect, text);
	}
	@Override
	public Dimension getPreferredSize(JComponent c) {
		Dimension result = super.getPreferredSize(c);
		result.width += slant * 2;
		return result;
	}

	private void initShape(JComponent c) {
		height = c.getHeight();
		width = c.getWidth();
		if (!c.getBounds().equals(base)) {
			base = c.getBounds();
			Polygon p = new Polygon();
			p.addPoint(slant, 0);
			p.addPoint(c.getWidth() - 1, 0);
			p.addPoint(c.getWidth() - 1 - slant, c.getHeight());
			p.addPoint(0, c.getHeight());
			shape = p;
		}
	}
	private void paintFocusAndRollover(Graphics2D g2, AbstractButton b, Color co) {
		paintFocusAndRollover(g2, b, co, co.brighter());
	}

	private void paintFocusAndRollover(Graphics2D g2, AbstractButton b, Color low, Color high) {
		g2.setPaint(new GradientPaint(0, 0, low, width, height, high, true));
		Polygon l = new Polygon();
		l.addPoint(slant, 0);
		l.addPoint(slant + indentSize, 0);
		l.addPoint(indentSize, height);
		l.addPoint(0, height);
		g2.fill(l);
		l = new Polygon();
		l.addPoint(slant + indentSize + actualIndentSpace, 0);
		l.addPoint(width - BASE_INDENT + actualIndentSpace, 0);
		l.addPoint(width - BASE_INDENT + actualIndentSpace - slant, height);
		l.addPoint(indentSize + actualIndentSpace, height);
		g2.fill(l);
		g2.setColor(b.getBackground());
	}
/*	private boolean isMouseOver(AbstractButton ab){
		return get(ab).isMouseOver;
	}
	private SpecialListener get(AbstractButton ab){
		return (SpecialListener)getButtonListener(ab);
	}*/
	class SpecialListener extends BasicButtonListener{
		//private Shape shape;
		private AbstractButton b;
		public SpecialListener(AbstractButton b, Shape s) {
			super(b);
			this.b = b;
			shape = s;
			animator.setDaemon(true);
			animator.start();
		}

		boolean isMouseOver;
		@Override
		public void mousePressed(MouseEvent e) {
			AbstractButton b = (AbstractButton) e.getSource();
			initShape(b);
			if (shape.contains(e.getX(), e.getY())) {
				super.mousePressed(e);
			}
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			if (shape.contains(e.getX(), e.getY())) {
				isMouseOver = true;
				super.mouseEntered(e);
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			if (shape.contains(e.getX(), e.getY())) {
				isMouseOver = true;
				super.mouseEntered(e);
			} else {
				isMouseOver = false;
				super.mouseExited(e);
			}
		}

		@Override
		public void mouseExited(MouseEvent e) {
			isMouseOver = false;
			super.mouseExited(e);
		}

		private Thread animator = new Thread(new Runnable() {
			public void run() {
				try {
					while (true) {
						Thread.sleep(100);
						if (isMouseOver || b.getModel().isPressed()) {
							doAnimation();
						}
					}
				} catch (InterruptedException ie) {
				}
			}
		}, "Animator");

		public void doAnimation() throws InterruptedException {
			for (actualIndentSpace = 0; actualIndentSpace < BASE_INDENT && (isMouseOver || b.getModel().isPressed()); actualIndentSpace++){
				b.repaint();
				Thread.sleep(animationSpeedFactor);
			}
			while (isMouseOver|| b.getModel().isPressed())
				Thread.sleep(animationSpeedFactor);
			for (; actualIndentSpace > 0; actualIndentSpace--){
				b.repaint();
				Thread.sleep(animationSpeedFactor);
			}
			b.repaint();
		}
	}
}
