package org.ace.gui;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import org.ace.util.Settings;
import org.ace.util.Util;

/**
 * Handles and disperses all images
 * @author Andrew
 *
 */
public class ImgRef {
	private static final String[][] planeNames = {
		{"F22", "PlaneEnemy.png", "PlaneFriendly.png", "PlaneR.png"},
		{"Biplane", "","", ""}
	};
	private static final String[][] weaponNames = {
		{"BulletEntity", "bullet.png"},
		{"HomingEntity", "MissileOriginal.png"},
		{"TurretEntity", ""}
		//{"Biplane", "",}
	};
	private static final BufferedImage[][] planeImgs;
	private static final BufferedImage[][] weaponImgs;
	private static final BufferedImage imgUnknown = Util.getRandomSCImage(10, 10);
	static{
		planeImgs = new BufferedImage[planeNames.length][];
		for(int i = 0; i < planeImgs.length; i++){
			planeImgs[i] = new BufferedImage[Math.max(1, planeNames[i].length-1)];
			for(int j = 0; j < planeImgs[i].length; j++){
				String tp = Settings.getTP();
				try{
					String a =  planeNames[i][j+1];
					if(tp != null)
						a = tp + a;
					planeImgs[i][j] = Util.getImage(a);
				}catch(Exception e){
					
				}
				if(planeImgs[i][j] == null){
					System.out.println("Loading different for "+ planeNames[i][0]);
					planeImgs[i][j] = Util.getRandomSCImage(10, 10);
				}
			}
		}
		weaponImgs = new BufferedImage[weaponNames.length][];
		for(int i = 0; i < weaponImgs.length; i++){
			weaponImgs[i] = new BufferedImage[Math.max(1, weaponNames[i].length-1)];
			for(int j = 0; j < weaponImgs[i].length; j++){
				String tp = Settings.getTP();
				try{
					String a =  weaponNames[i][j+1];
					if(tp != null)
						a = tp + a;
					weaponImgs[i][j] = Util.getImage(a);
				}catch(Exception e){
					
				}
				if(weaponImgs[i][j] == null){
					System.out.println("Loading different for " + weaponNames[i][0]);
					weaponImgs[i][j] = Util.getRandomSCImage(10, 10);
				}
			}
		}
		Graphics2D g2 = imgUnknown.createGraphics();
		g2.drawString("UK", 0, 10);
	}
	public static void validate(){
		System.out.println(planeImgs != null);
	}
	public static BufferedImage getProjectile(String s){
		for(int i = 0; i < weaponNames.length; i++){
			if(weaponNames[i][0].equals(s))
				return weaponImgs[i][0];
		}
		return null;
	}
	public static BufferedImage getImageSelf(String s){
		return getImage(s, 2);
	}
	public static BufferedImage getImageHostile(String s){
		return getImage(s, 0);
	}
	public static BufferedImage getImageFriendly(String s){
		return getImage(s, 1);
	}
	private static BufferedImage getImage(String s, int j){
		int i;
		for(i = 0; i < planeNames.length; i++)
			if(planeNames[i][0].equals(s)){
				return planeImgs[i][j];
			}
		return imgUnknown;
//		return null;
	}
}
