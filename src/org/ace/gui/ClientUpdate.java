package org.ace.gui;

import java.util.concurrent.atomic.AtomicBoolean;

import org.ace.entity.FlyingEntity;

import org.ace.network.Client;
import org.ace.network.Command;
import org.ace.network.CommandGiveEntity;
import org.ace.network.CommandHandshake;
import org.ace.network.CommandMove;
import org.ace.network.CommandSendDamage;
import org.ace.network.CommandSendMaxHealth;
import org.ace.network.CommandUpdate;

import org.ace.phys.Vector2D;
import org.ace.planes.Biplane;
import org.ace.planes.F22;
import org.ace.util.Settings;
/**
 * This is the client's update thread.
 * Helps to receive and send stuff, as well as calling act on things
 *
 */
public class ClientUpdate extends Thread{
	private static final long sleepTime = 1000 / 60;
	private long time;
	private volatile Client c;
	private GameGui gui;
	private FlyingEntity self;
	private Thread updateProcessor;
	private AtomicBoolean hasUpdate;
	private volatile boolean ended;
	
	public ClientUpdate(GameGui self){
		super("ClientUpdater");
		ended = false;
		gui = self;
		setDaemon(true);
		hasUpdate = new AtomicBoolean(false);
		updateProcessor = new Thread("UpdateProcessor for " + this) {
			public void run() {
				try {
					while(!updateProcessor.isInterrupted()){
						while (c != null) {
							time = System.currentTimeMillis();
							if(checkSelf()){
								ClientUpdate.this.self.act();
								hasUpdate.set(true);
								gui.repaint();
							}
							long st = sleepTime
									- (System.currentTimeMillis() - time);
							if (st > 0)
								Thread.sleep(st);
							else
								Thread.yield();
						}
						Thread.sleep(1000);
					}
				} catch (InterruptedException ie) {

				}
			}
		};
		updateProcessor.setDaemon(true);
		updateProcessor.start();
		start();
	}
	@Override
	public void run(){
		try{
		while(true){
			while(c != null){
				if(!c.isAlive()){
					end();
					break;
				}
				if(hasUpdate.getAndSet(false)){
					c.send(new CommandMove(self.getX(),self.getY(),self.getRotation(),self.isFiringBullets(),self.getMissileCount(),self.getVel()));
				}
				Command x = c.getUpdate();
				if(x == null){
					Thread.sleep(2); continue;
				}else if(x instanceof CommandHandshake){
					gui.handshake((CommandHandshake)x);
					respawn();
					gui.repaint();
				}else if(x instanceof CommandUpdate){
					gui.update((CommandUpdate)x);
					gui.repaint();
				}else if(x instanceof CommandSendDamage){
					self.setHealth(((CommandSendDamage)x).health);
					gui.updateHealth(self.getHealth());
				}else if(x instanceof CommandSendMaxHealth){
					gui.setMaxHealth(((CommandSendMaxHealth)x).maxHealth);
				}
				else{
					throw new RuntimeException();
				}
				//tell the client's plane to act
				Thread.yield();
				
			}
			//if there is no client, there are no updates to manage. Sleep.
			Thread.sleep(1000);
		}
		}catch(InterruptedException ie){
			//If this thread is interrupted, then it is intended for it to die.
			//LET IT JUST DIE ALREADY!
		}
	}
	/**
	 * Ties this GUI to the specified client. It can be changed.
	 * 
	 * @param c
	 *            The client to to tie the gui to.
	 */
	public void setClient(Client c){
		this.c = c;
	}
	
	public void respawn()
	{
		self = new F22(150 + Math.random() * Settings.getWidth(),-150 - Math.random() * 100,200,40);
		c.send(new CommandGiveEntity(self));
	}
	public void setThrust(int i){
		self.setThrust(i);
	}
	public int getX(){
		return round(self.getX());
	}
	public int getY(){
		return round(self.getY());
	}
	public int getHeight() {
		return round(self.getHeight());
	}
	public double getRotation() {
		return self.getRotation();
	}
	public int getWidth() {
		return round(self.getWidth());
	}
	private int round(double d){
		return (int)Math.round(d);
	}
	public void setRotationChangeOnAct(double radians) {
		self.setRotationChangeOnAct(radians);
	}
	public void setFire(boolean b) {
		self.setBulletFire(b);
	}
	public boolean isVisible() {
		return self.isVisible();
	}
	public boolean checkSelf() {
		return self != null;
	}
	public String getSelfString(){
		return self.toString();
	}
	public void setMissileCount(short s){
		self.setMissileCount(s);
	}
	public short getMissileCountNum(){
		return self.getMissileCount();
	}
	public void end() {
		if(ended)
			return;
		ended = true;
		c.stopThread();
		c = null;
		self = null;
		gui.leaveGame();
	}
	public String getSimpleName() {
		return self.getClass().getSimpleName();
	}
	public String getMissileCount() {
		short s = self.getMissileCount();
		if (s < 10)
			return "0"+ Short.toString(s);
		else
			return Short.toString(s);
	}
}
