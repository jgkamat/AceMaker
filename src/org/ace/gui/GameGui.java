package org.ace.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

import java.awt.image.BufferedImage;

import java.util.ArrayList;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JPanel;

import org.ace.entity.Entity;
import org.ace.entity.FlyingEntity;
import org.ace.network.CommandHandshake;
import org.ace.network.CommandUpdate;
import org.ace.phys.Vector2D;
import org.ace.util.Settings;
import org.ace.util.Util;


/**
 * This class is the gui of the actual game, it has a private thread which keeps
 * it up to date with the client, pushing anything received from the server to
 * the graphics.
 * 
 * @author Andrew
 * @version 2013.5.19
 */
public class GameGui extends JPanel{

	private static final long serialVersionUID = 2914543775700058503L;
	private static final float NEARBY_AREA = 0f;
	//private static final String SELF_IMAGE = "PlaneR.png";
	private static boolean initialized = false;

	private volatile CommandUpdate latestUpdate;
	private ArrayList<Entity> enviro;
	//private final Map<Class<? extends Entity>, Image> link;
	//private FlyingEntity self;

	//private Image selfImage;
	private BufferedImage background;

	private boolean debug = false, missilePressed=false;
	private final AtomicBoolean ab; //Debug feature: TODO: Remove?

	private int viewingRadius,
	xAdjust, yAdjust,
	selfX, selfY;

	private ClientUpdate self;
	private boolean isA, isD;
	private volatile short health = -1, maxHealth = -1;
	
	private volatile boolean isRespawning = false, finishedInit = false;

	/**
	 * A class to represent the game's GUI (of the client)
	 */
	public GameGui() {
		super(null);
		if(initialized)
			throw new RuntimeException("THERE CAN ONLY BE ONE!! " + this.getClass().getSimpleName());
		initialized = true;

		ab = new AtomicBoolean(false);
		//		link = new HashMap<Class<? extends Entity>, Image>();
		//selfImage = Util.getImage(SELF_IMAGE);
		//registerClass(FlyingEntity.class, Util.getImage("PlaneEnemy.png"));
		//		registerClass(F22.class, Util.getImage("PlaneEnemy.png"));
		self = new ClientUpdate(this);
		this.addKeyListener(new KeyAdapter(){ //TODO: Make it so the plane does this automatically.
			@Override
			public void keyPressed(KeyEvent e) {
				if(!finishedInit)
					return;

				int i = e.getKeyCode();
				switch (i) {
				case KeyEvent.VK_W:
					self.setThrust(1);
					break;
				case KeyEvent.VK_A:
					isA = true;
					self.setRotationChangeOnAct(-0.05);
					break;
				case KeyEvent.VK_D:
					isD = true;
					self.setRotationChangeOnAct(0.05);
					break;
				case KeyEvent.VK_J:
					self.setFire(true);
					break;
				case KeyEvent.VK_K:
					if(missilePressed==false){
						self.setMissileCount((short) (self.getMissileCountNum()-1));
						missilePressed=true;
					}
					break;
				case KeyEvent.VK_F12:
					debug = ! debug;
					break;
				//case KeyEvent.VK_ESCAPE:
				//	leaveGame();
				//	break;
				}
				// System.out.println("KP " + e.getExtendedKeyCode());
			}
			@Override
			public void keyReleased(KeyEvent e){
				if(!finishedInit)
					return;
				
				switch (e.getKeyCode()) {
				case KeyEvent.VK_W:
					self.setThrust(0);
					break;
				case KeyEvent.VK_A:
					isA = false;
					if(isD)
						self.setRotationChangeOnAct(0.05);
					else
						self.setRotationChangeOnAct(0);
					break;
				case KeyEvent.VK_D:
					isD = false;
					if(isA)
						self.setRotationChangeOnAct(-0.05);
					else
						self.setRotationChangeOnAct(0);
					break;
				case KeyEvent.VK_K:
					missilePressed=false;
					break;
				case KeyEvent.VK_J:
					self.setFire(false);
					break;
				}
				//System.out.println("KP " + e.getExtendedKeyCode());
			}
		});
		addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e){
				if(isRespawning){
					isRespawning = false;
					self.respawn();
					repaint();
				}
			}
		});
		this.requestFocus();
	}
	/**
	 * Exits and returns to the main menu.
	 */
	public void leaveGame(){
		finishedInit = false;
		background = null;
		latestUpdate = null;
		enviro = null;
		health = -1;
		maxHealth = -1;
		self.end();
		AceMaker.returnToMain();
	}

	/**
	 * Does some network stuff to get server+client to work nicely
	 * @param ch Command with the environment in it
	 */
	public void handshake(CommandHandshake ch){
		this.requestFocus();
		enviro = ch.enviroment;
		background = Util.getImage(ch.mapName);
		Settings.setWidth(background.getWidth(null));
		Settings.setHeight(background.getHeight(null));
		this.viewingRadius = ch.viewingRadius;
	}

	/**
	 * Updates the world (from the server)
	 * @param update Has updates in it
	 */
	public void update(CommandUpdate update){
		//this.requestFocus();
		if(latestUpdate == null){
			latestUpdate = update;
			return;
		}
		if(update.updateNum > this.latestUpdate.updateNum){
			latestUpdate = update;
			repaint();
		}
		ab.set(true);
	}

	private void adjustMap() {
		int width = getWidth(), height = getHeight();
		double xLeft = width * (1-NEARBY_AREA) /2;
		double xRight = width - xLeft;
		selfX = self.getX();
		selfY = self.getY();
		double currentX = selfX + xAdjust; //-self.getWidth()/2.0
		if (xLeft > currentX)
			xAdjust += xLeft- currentX;
		else if (xRight < currentX)
			xAdjust -= currentX - xRight;

		double up = height * (1-NEARBY_AREA) / 2, down = height - up;
		double currentY = selfY - yAdjust; //- self.getHeight()/2.0
		if(currentY < up)
			yAdjust += currentY - up;
		else if(currentY > down)
			yAdjust += currentY - down;
		if(yAdjust < 0){
			yAdjust = 0;
		}else if(background != null && yAdjust > background.getHeight() - this.getHeight()){
			yAdjust = background.getHeight() - this.getHeight();
		}
	}

	/**
	 * @see javax.swing.JComponent#paintComponent(Graphics g)
	 */
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		Graphics2D g2 = (Graphics2D)g;
		if(self.checkSelf())
			adjustMap();
		if (background != null) {
			//yAdjust = Math.max(0, (int) (self.getY() - this.getWidth() / 2.0));
			//xAdjust = 0;
			g2.drawImage(background, xAdjust,
					-yAdjust,
					null);
			if(xAdjust > 0){
				g2.drawImage(background, xAdjust - background.getWidth(), -yAdjust, null);
			}else if(xAdjust < background.getWidth()){
				g2.drawImage(background, xAdjust + background.getWidth(), -yAdjust, null);
			}
			if(self.checkSelf()){
				g2.setColor(new Color(255,255,255,50));
				g2.fillOval((int)selfX - viewingRadius + xAdjust, (int)selfY- viewingRadius - yAdjust,viewingRadius*2,viewingRadius*2);
			}
		}
		if (enviro != null) {
			for (Entity e : enviro) {
				//if(e.isVisible()){
				//link.get(e.getClass());
				Image i = ImgRef.getProjectile(e.getClass().getSimpleName());
				if(i == null)
					i = ImgRef.getImageHostile(e.getClass().getSimpleName());
				g2.drawImage(i,
						(int) Math.round(e.getX() + e.getWidth() / 2.0),
						(int) Math.round(e.getY() + e.getHeight() / 2.0), null);
				// }
			}
		}
		if (latestUpdate != null) {
			for (Entity e : latestUpdate.updates) {
				this.drawEntity(g2, e);
				//if(e.isVisible()){
				/*AffineTransform temp=g2.getTransform();
				g2.rotate(e.getRotation(),(int) Math.round(e.getX()) + (int)xAdjust,(int)Math.round(e.getY())- (int)yAdjust);
				Image i = ImgRef.getProjectile(e.getClass().getSimpleName());//link.get(e.getClass());
				if(i == null)
					i = ImgRef.getImageHostile(e.getClass().getSimpleName());
				/*if(i == null){
						registerClass(e.getClass());
						i = link.get(e.getClass());
					} * /
				if(e.getRotation()>Math.PI/2&&e.getRotation()<3*Math.PI/2){
					g2.drawImage(i, (int) Math.round(e.getX()-e.getWidth()/2.0) + (int)xAdjust, 
							(int)Math.round(e.getY()+e.getHeight()/2.0)- (int)yAdjust,(int)e.getWidth(),-(int)e.getHeight(), null);
				}
				else{
					g2.drawImage(i, (int) Math.round(e.getX()-e.getWidth()/2.0) + (int)xAdjust, 
							(int)Math.round(e.getY()-e.getHeight()/2.0)- (int)yAdjust,(int)e.getWidth(),(int)e.getHeight(), null);
				}
				g2.setTransform(temp);*/
				//g2.drawImage(i, (int) Math.round(e.getX()-e.getWidth()/2.0), 
				//		(int)Math.round(e.getY()-e.getHeight()/2.0), null);
				//g2.drawImage(link.get(e.getClass()), (int) Math.round(e.getX()+e.getWidth()/2.0), 
				//		(int)Math.round(e.getY()+e.getHeight()/2.0), null);
				//}
			}
		}
		if(self.checkSelf()&&self.isVisible()){
			int x, y, wi, he;
			
			double rot = self.getRotation();
			boolean adjusted = false;
			if(rot-0.015 < 3 * Math.PI / 2 && rot-0.015 > Math.PI / 2){
				rot -= Math.PI; 
				adjusted = true;
			}
			x = selfX - self.getWidth() / 2 + xAdjust;
			y = selfY - self.getHeight() / 2 - yAdjust;
			wi = self.getWidth();
			he = self.getHeight();
			final int indent = 15, height = 12, healthWid = 100;
			final int yM = Math.min(y, y + he) - height;

			AffineTransform temp=g2.getTransform();
			g2.rotate(rot, selfX + xAdjust, selfY
					- yAdjust);
			g2.setColor(Color.DARK_GRAY);
			g2.fillRect(x, yM, healthWid + indent, height);
			g2.setColor(Color.BLACK);
			g2.fillRect(x + indent, yM, healthWid, height);
			g2.setColor(Color.RED);
			g2.fillRect(x + indent, yM, health * healthWid / maxHealth, height);
			g2.setColor(Color.WHITE);
			g2.drawString(self.getMissileCount() + " "+ health + "/" + maxHealth, x, yM + height - 1);
			
			if(adjusted){
				x += wi;
				wi *= -1;
			}
			
			g2.drawImage(
					ImgRef.getImageSelf(self.getSimpleName()),
					x,
					y,
					wi,
					he,
					null);

			g2.setTransform(temp);
			//System.out.print(-(int)Math.round(self.getY()-self.getHeight()/2.0) + ", ");
		}
		else{
			//TODO construct new self? Maybe not here
		}
		
		if(isRespawning)
			drawRespawn(g2);
		//TODO: Draw other things.
		if(debug)
			drawDebug(g);
	}

	private void drawRespawn(Graphics2D g2) {
		Font tm = g2.getFont(), f1 = new Font("Dialog", Font.BOLD, tm.getSize()), f2 = tm,
				f3 = new Font("Dialog", Font.PLAIN, (int) (tm.getSize() * 6.5));
		String s1 = "You have died!", s2 = "click anywhere to respawn", s3 = "RESPAWN";
		g2.setColor(new Color(100, 100, 100, 50));
		g2.fillRect(0, 0, getWidth(), getHeight());
		g2.setColor(Color.WHITE);
		FontMetrics fm;
		Rectangle2D r;
		int lastH = 100;
		
		g2.setFont(f1);
		fm = g2.getFontMetrics();
		r = fm.getStringBounds(s1, g2);
		lastH += (int)r.getHeight();
		g2.drawString(s1, (getWidth() - (int)r.getWidth())/2, lastH);
		
		g2.setFont(f2);
		fm = g2.getFontMetrics();
		r = fm.getStringBounds(s2, g2);
		lastH += (int)r.getHeight();
		g2.drawString(s2, (getWidth() - (int)r.getWidth())/2,lastH);
		
		
		g2.setFont(f3);
		fm = g2.getFontMetrics();
		r = fm.getStringBounds(s3, g2);
		lastH += (int)r.getHeight();
		int x = (getWidth() - (int)r.getWidth())/2, y = lastH, wid = (int)r.getWidth(), hei = (int)r.getHeight();
		
		g2.drawString(s3, x, y);
	}
	private void drawEntity(Graphics2D g2, Entity e){
		int x, y, wi, he;
		
		double rot = e.getRotation();
		boolean adjusted = false;
		if(rot-0.015 < 3 * Math.PI / 2 && rot-0.015 > Math.PI / 2){
			rot -= Math.PI; 
			adjusted = true;
		}
		x = checkedCast(e.getX() - e.getWidth() / 2) + xAdjust;
		y = checkedCast(e.getY() - e.getHeight() / 2) - yAdjust;
		wi = Math.max(checkedCast(e.getWidth()), 5);
		he = Math.max(checkedCast(e.getHeight()), 5);
		
		final int indent = 15, height = 12, healthWid = 100;
		final int yM = Math.min(y, y + he) - height;

		AffineTransform temp=g2.getTransform();
		if(e instanceof FlyingEntity){
			FlyingEntity e2 = (FlyingEntity)e;
			int health = e2.getHealth();
			int maxHealth = 1000;
			g2.rotate(rot, checkedCast(e.getX()) + xAdjust, checkedCast(e.getY()) - yAdjust);
			g2.setColor(Color.DARK_GRAY);
			g2.fillRect(x, yM, healthWid + indent, height);
			g2.setColor(Color.BLACK);
			g2.fillRect(x + indent, yM, healthWid, height);
			g2.setColor(Color.RED);
			g2.fillRect(x + indent, yM, health * healthWid / maxHealth, height);
			g2.setColor(Color.WHITE);
			g2.drawString(self.getMissileCount() + " "+ health + "/" + maxHealth, x, yM + height - 1);
		}
		if(adjusted){
			x += wi;
			wi *= -1;
		}
		Image i = ImgRef.getProjectile(e.getClass().getSimpleName());
		if(i == null)
			i = ImgRef.getImageHostile(e.getClass().getSimpleName());
		g2.drawImage(
				i,
				x,
				y,
				wi,
				he,
				null);

		g2.setTransform(temp);
	}
	private int checkedCast(double d){
		return Double.valueOf(d).intValue();
	}
	/**
	 * Helps us do things without us printing out nasty stuff
	 * @param g I have no idea what this does. Probably doesn't help with drawing things in any way, though.
	 */
	private void drawDebug(Graphics g){
		if(self.checkSelf()){
			Vector2D v = new Vector2D(selfX,selfY, self.getRotation(), 80, false);
			//System.out.println(self.getX() + ", "  +self.getY());
			//System.out.println((int)v.getY1());
			g.setColor(Color.RED);
			g.drawLine((int)v.getX1() + xAdjust,(int)v.getY1() - yAdjust,(int) v.getX2() + xAdjust, (int)v.getY2()- yAdjust);
		}
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, 200, 50);
		g.setColor(Color.BLACK);
		g.drawString("Background = " +background, 0, 10);
		int j, k;
		if(enviro == null){
			j = 0;
		}else{
			j = enviro.size();
		}if(latestUpdate == null){
			k = 0;
		}else{
			k = latestUpdate.updates.length;
		}
		g.drawString("E = " + j + ", LU = " + k, 0, 20);
		if(self.checkSelf())
			g.drawString("Self = " + self.getSelfString(), 0,30);
		g.drawString("Updated = " + ab.getAndSet(false), 0, 40);
		g.drawString("xAd=" + xAdjust + ",yAdj=" + yAdjust, 0, 50);
	}

	/**
	 * Gets the clients plane.
	 * @return
	 */
	public ClientUpdate getUpdater(){
		return this.self;
	}
	public void updateHealth(short health) {
		this.health = health;
		if(this.health <= 0)
			isRespawning = true;
	}
	public void setMaxHealth(short health2) {
		this.maxHealth = health2;
		finishedInit = true;
	}


}
