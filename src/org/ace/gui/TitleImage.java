package org.ace.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MediaTracker;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.Timer;

import org.ace.util.Util;

/**
 * Its the images in the title
 * @author Andrew
 *
 */
class TitleImage extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4925592706421827907L;
	private transient BufferedImage pic1, pic2;
	// private transient BufferedImage draw1,draw2;
	private boolean one;
	private final double targetRatio;
	private double sourceRatio;
	private int reqW, reqH, locX, locY;

	public TitleImage(JComponent higher) {
		pic1 = Util.getImage("Title1.png");
		pic2 = Util.getImage("Title2.png");
		MediaTracker mt = new MediaTracker(this);
		mt.addImage(pic1, 1);
		mt.addImage(pic2, 1);
		new Timer(600, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				one = !one;
				TitleImage.this.repaint();
			}
		}).start();
		one = true;
		try {
			mt.waitForAll();
		} catch (InterruptedException ex) {
			Logger.getLogger(TitleImage.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		// if(pic1 == null || pic2 == null)
		// throw new Exception();
		targetRatio = pic1.getWidth() / (double) pic1.getHeight();
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				updateImage();
			}
		});
		higher.addComponentListener(new ComponentAdapter(){
			public void componentResized(ComponentEvent e){
				int wid = e.getComponent().getWidth();
				setPreferredSize(new Dimension(wid, getHeightForWidth(wid)));
			}
		});
		Dimension commDim = new Dimension(pic1.getWidth(), pic1.getHeight());
		//setMaximumSize(commDim);
		setPreferredSize(commDim);
		//setMinimumSize(new Dimension(pic1.getWidth() / 10,
		//		pic1.getHeight() / 10));
		setOpaque(false);
	}
	public int getHeightForWidth(int width){
		return pic1.getHeight() * width / pic1.getWidth();
	}
	private void updateImage(){
		sourceRatio = getWidth() / (double) getHeight();
		if (sourceRatio <= targetRatio) {
			reqW = getWidth();
			reqH = (int) (reqW / targetRatio);
			locY = (getHeight() - reqH) / 2;
			locX = 0;
		} else {
			reqH = getHeight();
			reqW = (int) (reqH * targetRatio);
			locY = 0;
			locX = (getWidth() - reqW) / 2;
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (one)
			g.drawImage(pic1, locX, locY, reqW, reqH, this);
		else
			g.drawImage(pic2, locX, locY, reqW, reqH, this);
	}
}