package org.ace;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.ace.gui.AceMaker;
import org.ace.gui.ImgRef;
import org.ace.gui.ServerHead;
import org.ace.network.NetworkServer;
import org.ace.util.Util;

/**
 * The main method. Wohoo!
 * 
 */
public class Main {
	private volatile static NetworkServer s;

	public static void main(String... argv) throws Exception {
		org.log.LoggingUtil.setup();
		
		//String s = JOptionPane.showInputDialog("Enter params");
		//if (s != null)
		//	argv = s.split(" ");
		if (argv.length == 0 || argv[0].equals("") || argv[0].equals("-client")) {
			try {
				ImgRef.validate();
				AceMaker am = new AceMaker();
				am.setSize(800, 600);
				am.setLocationRelativeTo(null);
				am.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				am.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(1);
			}
		} else if (argv[0].equals("-server")) {
			// if(argv.length == 1){
			// test();
			Util.setDefaultLAF();
			ServerHead sh = new ServerHead();
			sh.setTitle("Server");
			sh.setSize(800, 600);
			sh.setLocationRelativeTo(null);
			sh.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			sh.setVisible(true);
			// s = new NetworkServer(Util.getDefaultPort(), true);
			// return;
			// }else if (argv.length == 2){

			// s = new NetworkServer(Util.getDefaultPort(), true, argv[1]);
			// return;
			// }
		} else
			usage();
		Util.logI("Main dying: Printing threads");
		Util.printThreads();
	}

	/**
	 * How the program scolds the user that he has entered somthing wrong
	 */
	public static void usage() {
		System.out.println("Usage: [-client | -server [settingsFile]] ");
	}
}
